<?php

	/*
	|--------------------------------------------------------------------------
	| Administrator panel settings
	|--------------------------------------------------------------------------
	|
	| Change this settings to desire name and defaults
	| this will be the url for administration CMS
	|
	|
	*/
	# config/module.php

	return  [
	    'modules' => [
	    		'Admin' => [
		       		// Controller
		       		'Controller' => '',
		       		// Model
		       		'Model' => ''
		       	],
		       	'Banner' => [
		       		// Controller
		       		'Controller' => ['Banners'],
		       		// Model
		       		'Model' => ['Banner']
		       	],
		       	'Blog' => [
		       		// Controller
		       		'Controller' => ['Blogs','BlogCategories'],
		       		// Model
		       		'Model' => ['Blog','BlogCategory']
				],
				'News' => [
					// Controller
					'Controller' => ['NewsController','NewsCategories'],
					// Model
					'Model' => ['News','NewsCategory']
				],
		       	'Campaign' => [
		       		// Controller
		       		'Controller' => ['Campaigns','Videos','Ambassadors','Events'],
		       		// Model
		       		'Model' => ['Campaign','Video','Ambassador','Event']
		       	],
		       	'Career' => [
		       		// Controller
		       		'Controller' => ['Applicants','Careers','Divisions'],
		       		// Model
		       		'Model' => ['Applicant','Career','Division']
		       	],
		       	'Contact' => [
		       		// Controller
		       		'Controller' => ['Contacts'],
		       		// Model
		       		'Model' => ['Contact']
		       	],
		       	'Employee' => [
		       		// Controller
		       		'Controller' => ['Employees'],
		       		// Model
		       		'Model' => ''
		       	],
		       	'Page' => [
		       		// Controller
		       		'Controller' => ['Menus','Pages'],
		       		// Model
		       		'Model' => ['Menu','Page']
		       	],
		       	'Participant' => [
		       		// Controller
		       		'Controller' => ['Participants'/*,'Images'*/,'Votes'],
		       		// Model
		       		'Model' => ['Participant','Images','Votes']
		       	],
		       	'Portfolio' => [
		       		// Controller
		       		'Controller' => ['Portfolios','Projects','Clients'],
		       		// Model
		       		'Model' => ['Portfolio','Project','Client']
		       	],
		       	'Task' => [
		       		// Controller
		       		'Controller' => ['Task'],
		       		// Model
		       		'Model' => ''
		       	],
		       	'User' => [
		       		// Controller
		       		'Controller' => ['Users','Roles','Teams','Settings'],
		       		// Model
		       		'Model' => ['User','Role','Team','RoleUser','Setting']
		       	]
	    ]
	];
