@extends('layouts.app')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">				
				<div class="panel-heading sr-only">Login</div>
				<div class="panel-body py-4">
					@if (count($errors) > 0)
					<div class="container">
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					</div>
					@endif
					{!! Form::open([
						'route' => 'auth.login',
						'class' => 'from-horizontal',
						'method' => 'POST'
					]) !!}	
						<div class="form-group">
							<label class="col-md-4 control-label">{{ trans('label.email_address') }}</label>
							<div class="col-md-12">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('label.email_address') }}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-4 control-label">{{ trans('label.password') }}</label>
							<div class="col-md-12">
								<input type="password" class="form-control" name="password" placeholder="{{ trans('label.password') }}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> {{trans('label.remember_me')}}
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 col-md-offset-4">
								<button type="submit" class="btn btn-primary">{{ trans('label.login') }}</button>
								<a class="btn btn-link" href="{{ url('/auth/password/email') }}">{{ trans('label.forgot_password') }}</a>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-12 col-xs-12 col-md-offset-4">
								Social Login :
								<a href="{{ url('auth/social/twitter') }}" title="{{ trans('label.login_with') }} Twitter" class="btn btn-info btn-sm"><span class="fab fa-twitter"></span>&nbsp; Twitter</a>
								<a href="{{ url('auth/social/facebook') }}" title="{{ trans('label.login_with') }} Facebook" class="btn btn-primary btn-sm"><span class="fab fa-facebook"></span>&nbsp; Facebook</a>
								<a href="{{ url('auth/social/linkedin') }}" title="{{ trans('label.login_with') }} Linkedin" class="btn btn-success btn-sm"><span class="fab fa-linkedin"></span>&nbsp; LinkedIn</a>
								<a href="{{ url('auth/social/google') }}" title="{{ trans('label.login_with') }} Google" class="btn btn-danger btn-sm"><span class="fab fa-google"></span>&nbsp; Google</a>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
