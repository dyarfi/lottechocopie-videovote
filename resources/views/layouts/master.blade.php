<!DOCTYPE html>
<html dir="ltr" lang="{{ app()->getLocale() }}">
<head>
<meta name="author" content="{{ @$company_name->value }}" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- SEO tag -->
<meta property="og:url" content="{{ url()->current() }}" />
@if(isset($ogs))
@foreach ($ogs as $og => $graph)
<meta property="{{$og}}" content="{{ $graph }}" />
@endforeach
@endif
<meta property="og:locale" content="en_US" />
<meta property="og:site_name" content="{{ @$company_name->value }}" />
<meta property="og:type" content="website" />
<meta property="fb:app_id" content="1911132052253936" />
<meta name="keyword" content="{{ $meta_keywords->value }}" />
<meta name="description" content="{{ @$meta_description->value }}" />
<link rel="canonical" href="{{ url()->current() }}" />
<script type="application/ld+json">
{"description":"{{@$meta_description->value}}","@type":"WebPage","url":"{{url()->current()}}","publisher":{"@type":"Organization","logo":{"@type":"ImageObject","url":"{{asset('images/logo/logo.png')}}"}},"headline":"{{@$title .' - '. @$company_name->value}}","@context":"http://schema.org"}
</script>
<!-- End SEO tag -->
<title>{{ @$title .' - '. @$company_name->value }}</title>
<link href="https://fonts.googleapis.com/css?family=Montserrat|Merriweather" rel="stylesheet">
@routes
<link rel="stylesheet" href="{{ mix('css/app.bundle.css') }}">
@if(isset($styles)) @foreach ($styles as $style => $css) {!! Html::style($css, ['rel'=>'stylesheet']) !!} @endforeach @endif
    <script type="text/javascript">var base_URL = "{{ url('/') }}/";</script>
    <style media="screen">
        .bg-custom-nav { background-size: contain; position: fixed; width: 100%; z-index: 99; top: 0; left: 0; }
        @media screen and (max-width: 767px) { .row { margin: 0; } .bg-custom-nav .img-fluid { max-width: 60%; } }
        #wrapper { padding-top: 50px; }
        .owl-carousel .owl-item .owl-lazy { opacity: 1; }
    </style>
</head>
<body class="main-body">
<div id="wrapper" class="main-page">
    <nav class="navbar navbar-expand-lg navbar-top navbar-dark bg-custom-nav">
        <div class="container">
            @if($logo = app('App\Modules\User\Model\Setting')->key('logo')->value)
            <a class="navbar-brand ml-lg-5 ml-0 mr-auto" href="{{ route('/') }}"><img class="img-fluid" src="{{ File::exists(public_path('images/logo/'.$logo)) ? asset('images/logo/'.$logo) : asset('images/logo.png') }}" alt="Logo"></a>
            @else
                <a class="navbar-brand" href="#">Logo</a>
            @endif
            <button class="navbar-toggler navbar-toggler-right navbar-icon" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="icon-bar bar1"></span><span class="icon-bar bar2"></span><span class="icon-bar bar3"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarText">
                <ul class="navbar-nav mr-3">
                    @foreach ($menus as $menu)
                        <li class="nav-item {{ $menu->slug == Route::current()->getName() || str_is(Request::segment(1), $menu->slug)? 'active current' : '' }}">
                            <a class="nav-link" href="{{ $menu->slug == 'home' ? route('/') : route($menu->slug) }}">{{ $menu->name }}</a>
                        </li>
                    @endforeach
                </ul>
                <span class="navbar-text ml-0 ml-lg-2">
                    <ul class="navbar-nav">
                        {{-- <li class="nav-item align-self-center">
                            <a class="nav-link" href="javascript:;" title="search" id="trigger-overlay"><i class="fas fa-search"></i></a>
                        </li>
                        <li class="nav-item d-none d-md-none d-lg-block d-xl-block"><div class="dropdown-divider"></div></li> --}}
                        @if (!Auth::check())
                            <li class="nav-item">
                                <a data-fancybox data-src="#hidden-content" href="javascript:;" class="form-handler nav-link" id="test">{{ trans('label.login') }}</a>
                                <div class="mx-auto">
                                    <div class="bg-brown text-warning" id="hidden-content" style="display:none">
                                        <div class="login-handler">
                                            <h4 class="container font-weight-bold py-2">Login</h4>
                                            {!! Form::open([
                                                'route' => 'auth.login',
                                                'class' => 'from-horizontal',
                                                'id'    => 'form-login',
                                                'method' => 'POST'
                                            ]) !!}
                                                <div class="container"><div class="message"></div></div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label class="control-label sr-only">{{ trans('label.email_address') }}</label>
                                                        <div class="input-group input-group-lg mb-3">
                                                            <div class="input-group-prepend"><span class="input-group-text text-warning"><i class="fa fa-envelope"></i></span></div>
                                                            <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ trans('label.email_address') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <label class="control-label sr-only">{{ trans('label.password') }}</label>
                                                        <div class="input-group input-group-lg mb-3">
                                                            <div class="input-group-prepend"><span class="input-group-text text-warning"><i class="fa fa-lock"></i></span></div>
                                                            <input type="password" class="form-control" name="password" placeholder="{{ trans('label.password') }}">
                                                            <div class="input-group-append"><span class="input-group-text text-warning"><a class="text-warning small d-none" href="{{route('auth.password')}}">Lupa password?</a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <div class="col-md-12">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="remember"> {{trans('label.remember_me')}}
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="my-0">
                                                    <div class="col-md-12 text-center">
                                                        <button type="submit" class="btn bg-button-alt-warning btn-md btn-rounded btn-login">{{ trans('label.login') }}</button>
                                                        <div class="text-white mt-3">
                                                            Belum punya akun?
                                                            <a class="text-warning register-trig" href="javascript:;" data-rel="{{ route('auth.register') }}">
                                                            Sign Up
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="container position-relative pb-3">
                                                    <div class="text-center text-warning">Atau</div>
                                                    <div class="text-center text-white pb-3">Login dengan Sosial Media</div>
                                                    <div class="row">
                                                        <div class="col-md-12 col-xs-12 text-center">
                                                            <a href="{{ url('auth/social/facebook') }}" title="{{ trans('label.login_with') }} Facebook" class="btn btn-primary btn-md btn-rounded"><span class="fab fa-facebook"></span>&nbsp; Facebook</a>
                                                            {{-- <a href="{{ url('auth/social/twitter') }}" title="{{ trans('label.login_with') }} Twitter" class="btn btn-info btn-md btn-rounded"><span class="fab fa-twitter"></span>&nbsp; Twitter</a>
                                                            <a href="{{ url('auth/social/linkedin') }}" title="{{ trans('label.login_with') }} Linkedin" class="btn btn-success btn-lg btn-rounded"><span class="fab fa-linkedin"></span>&nbsp; LinkedIn</a>
                                                            <a href="{{ url('auth/social/google') }}" title="{{ trans('label.login_with') }} Google" class="btn btn-danger btn-lg btn-rounded"><span class="fab fa-google"></span>&nbsp; Google</a> --}}
                                                        </div>
                                                    </div>
                                                </div>
                                            {!! Form::close() !!}
                                        </div>
                                        <div class="register-handler text-warning" style="display:none;">
                                            <div class="container">
                                                <h4 class="font-weight-bold py-2">Register</h4>
                                                {!! Form::open([
                                                    'route' => 'auth.register',
                                                    'class' => 'from-horizontal',
                                                    'id'    => 'form-register',
                                                    'method' => 'POST'
                                                ]) !!}
                                                <div class="container"><div class="message"></div></div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-lg-6">
                                                            <label class="control-label">{{ trans('label.first_name') }}</label>
                                                            <input type="text" class="form-control" placeholder="First Name" name="first_name" value="{{ old('first_name') }}">
                                                        </div>
                                                        <div class="col-lg-6">
                                                            <label class="control-label">{{ trans('label.last_name') }}</label>
                                                            <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{ old('last_name') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-lg-12">
                                                            <label class="control-label">{{ trans('label.email_address') }}</label>
                                                            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ old('email') }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-lg-12">
                                                            <label class="control-label">{{ trans('label.password') }}</label>
                                                            <input type="password" class="form-control" placeholder="Password" name="password">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-lg-12">
                                                            <label class="control-label">{{ trans('label.confirm_password') }}</label>
                                                            <input type="password" class="form-control" placeholder="Confirm Password" name="password_confirmation">
                                                        </div>
                                                    </div>
                                                </div>
                                                {{-- <div class="form-group{{ $errors->first('g-recaptcha-response', ' has-error') }}">
                                                    <div class="form-row">
                                                        <div class="col-lg-12">
                                                            {!! Form::label('captcha', 'Captcha', ['class'=>'control-label']); !!}
                                                            {!! app('captcha')->display(); !!}
                                                            <span class="help-block">{{{ $errors->first('g-recaptcha-response', ':message') }}}</span>
                                                        </div>
                                                    </div>
                                                </div> --}}
                                                <div class="form-group">
                                                    <div class="form-row">
                                                        <div class="col-lg-12">
                                                            <button type="submit" class="btn bg-button-alt-warning btn-rounded btn-login">{{ trans('label.register') }}</button>
                                                            <div class="align-self-center py-3">
                                                                Sudah punya akun? <a href="javascript:;" class="login-trig text-white">Login</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close(); !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        {{-- <li class="nav-item">
                            {{ trans('label.register_text') }} <a class="nav-link" href="{{ route('register') }}">{{ trans('label.register') }}</a>
                        </li> --}}
                        @else
                            <li class="nav-item align-self-center dropdown">
                                <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" id="dropdownMenu2" role="button">
                                    {!! Auth::check() ? '<i class="fas fa-user"></i> ' : '' !!}
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu" aria-labelledby="dropdownMenu2">
                                    {{-- <li class="nav-item"><a class="nav-link" href="{{ route('profile') }}">{{ trans('label.profile') }}</a></li> --}}
                                    <li class="nav-item"><a class="nav-link" href="{{ route('auth.logout','ref=true') }}">{{ trans('label.logout') }}</a></li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </span>
            </div>
        </div>
    </nav>
    <div class="position-relative"><div class="bg-traps-handler-top"><div class="bg-traps-top"><div class="bg-traps-content-top"></div></div></div></div>
    <div class="container px-0 mt-5 bg-shadow">
        <div class="container">
            @include('partials.alerts.success')
            @include('partials.alerts.errors')
        </div>
        <div class="bg-white">
            <div class="mt-4">
                @yield('content')
            </div>
        </div>
    </div>
    <div class="position-relative"><div class="bg-traps-handler"><div class="bg-traps"><div class="bg-traps-content"></div></div></div></div>
    <footer id="footer">
        <div class="container-fluid">
            <nav class="navbar navbar-dark justify-content-between">
                <ul class="navbar-nav">
                    <li class="nav-item"><span class="text-uppercase text-white"> {{ trans('label.copyright') }} {{ date('Y') }} &middot; {{ $company_name->value }}</span></li>
                </ul>
                <ul class="navbar-nav navbar-footer">
                    @foreach ($socials as $social)
                    <li class="nav-item"><a href="{{ $social->value }}" class="nav-link" title="{{ $social->name }}" target="_blank"><i class="fab fa-{{ $social->key }} fa-2x"></i></a></li>
                    @endforeach
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item"><a href="javascript:;" class="nav-link">lottechocopieindonesia.com <i class="fas fa-globe"></i></a></li>
                </ul>
            </nav>
        </div>
    </footer>
</div>
 <!-- Overlay Search -->
<div class="overlay overlay-hugeinc" id="overlay-search">
    <button type="button" class="overlay-close font-weight-light"><span class="fa fa-times-circle fa-3x"></span></button>
    <div class="navbar-search">
        <ul class="list-inline col-lg-12">
        <li class="list-inline-item">
            {!! Form::open(['route' => 'search.post','method' => 'POST','class' => 'form-inline container','id' => 'form-search']) !!}
            <div class="col-lg-12">
                <label class="sr-only" for="search-bar">Search</label>
                <div class="input-group input-group-lg mb-0 mb-sm-0">
                <div class="input-group-prepend"><span class="input-group-text"><i class="fa fa-search"></i></span></div>
                <input type="text" class="form-control" name="query" id="search-bar" placeholder="Type and hit Enter..">
                </div>
            </div>
            {!! Form::close() !!}
        </li>
        </ul>
    </div>
</div>
 <!-- Boot Modal -->
<div class="modal fade" id="BootModal" tabindex="-1" role="dialog" aria-labelledby="BootModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content mt-5" style="background-color:transparent;border:0">
            <div class="modal-header border-0">
                <button type="button" class="close btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true" class="text-white"><i class="fas fa-times fa-1x"></i></span>
                </button>
                {{--  <h3 class="modal-title text-primary" id="BootModalLabel">Modal title</h3>  --}}
            </div>
            <div class="modal-body p-0">
                <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" allowfullscreen></iframe>
                </div>
                <div class="content"></div>
            </div>
            <div class="modal-footer d-none">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
{{-- <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5ace6daaad779886"></script> --}}
<script src="{{ mix('js/app.bundle.js') }}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-42489917-28"></script>
<script> window.dataLayer = window.dataLayer || []; function gtag(){dataLayer.push(arguments);} gtag('js', new Date()); gtag('config', 'UA-42489917-28'); </script>
@if(Session::has('flash_thankyou'))
<script>
$(document).ready(function() {
    var addthis_share = {
        url: route('activity'),
        title: "#PremiumMomentstogether",
        description: "Voted",
        media: baseUrl + '/images/logo.png'
    }

    var thanks = 'Thank You for Voting, Mom! Terima kasih telah turut merayakan pencapaian baru si Kecil. Pemenang yang beruntung akan mendapat Grand Prize & hadiah menarik lainnya!';
    var shares = '<div class="row col"><a onClick="gtag(\'event\', \'share\', { \'event_category\': \'social\', \'event_label\': \'facebook\' })" target="_blank" href="https://www.facebook.com/dialog/feed?app_id=184683071273&amp;link=https%3A%2F%2Flottechocopieindonesia.com%2Factivity&amp;picture=https%3A%2F%2Flottechocopieindonesia.com%2Fimages%2Fimg-banner-activity.jpg&amp;name=Terima%20Kasih&amp;caption=%20&amp;description=Terima%20kasih%20telah%20memilih%20video%20ini.%20Ayo%20share%20link-nya%20%26%20ajak%20para%20Mom%20mengapresiasi%20%20pencapaian%20si%20Kecil!&amp;redirect_uri=http%3A%2F%2Fwww.facebook.com%2F" title="Login with Facebook" class="btn btn-primary btn-md btn-rounded mt-3 mr-2" > <span class="fab fa-facebook"></span>&nbsp; Facebook </a>'
    +'<a onClick="gtag(\'event\', \'share\', { \'event_category\': \'social\', \'event_label\': \'twitter\' })" title="Tweet" target="_blank" href="http://twitter.com/intent/tweet?text=Terima%20kasih%20telah%20memilih%20video%20ini.%20Ayo%20share%20link-nya%20%26%20ajak%20para%20Mom%20mengapresiasi%20%20pencapaian%20si%20Kecil!%20http%3A%2F%2Flottechocopieindonesia.com%2Factivity" class="btn btn-info btn-md btn-rounded mt-3"> <span class="fab fa-twitter"></span>&nbsp; Twitter</a></div>';
    var div = '<div class="bg-brown thanks-pop"><h2 class="main-head-alt">Thank you for your Vote!</h2><div class="row"><div class="col-sm-6"><img src="'+baseUrl+'images/img-thank-you.png" alt="Thank You" class="img-fluid"></div><div class="col-sm-6 text-white"><h4 class="main-head-alt">Share</h4>'+thanks+' '+shares+'</div></div></div>';
    $.fancybox.open({
        'src' : div,
        'type':'html'
    });
});
</script>
@endif
@if(isset($scripts)) @foreach($scripts as $script => $js) {!! Html::script($js, ['rel'=>$script]) !!} @endforeach @endif
</body>
</html>
