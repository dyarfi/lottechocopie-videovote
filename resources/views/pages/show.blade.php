@extends('layouts.master')

@section('content')
<section id="content" class="p-5">
    <div class="container m-5">
        <div class="col-lg-12">
            <h1>{{ $page->name }}</h1>
            <div>
                {!! $page->description !!}
            </div>
        </div>
    </div>
</section>
@stop
