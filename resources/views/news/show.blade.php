@extends('layouts.master')

@section('content')
<div class="position-relative">
	<div class="container-fluid">
		<div class="position-absolute abs-lrz-0">
			<div class="bg-brown pb-26"></div>
			<div class="bg-wave-brown-down"></div>
		</div>
		<div class="position-relative">
			<div class="container d-block">
				<div class="col-lg-12 mx-auto">					
					<div class="col-lg-10 mx-auto pt-4">
						<a href="{{route('news_event')}}" title="News Event" class="btn btn-outline-light btn-rounded text-uppercase">News</a>
						<h2 class="main-head-alt text-shadow-black">{{$news->name}}</h2>
						<div class="text-white mb-4">{{$news->created_at}}</div>
					</div>
					<div class="col-lg-12">
						@if(File::exists(public_path('uploads/'.$news->image)))
							<a href="#"><img src="{{ asset('uploads/'.$news->image) }}" alt="{{$news->name}}" class="img-fluid ilist-c"></a>
						@endif
					</div>
					<div class="col-lg-10 mx-auto">					
						<div class="py-5">						
							{!! $news->description !!}
							<div class="line"></div>
							<div class="row">
								<div class="col-12">
									<div class="float-right">
										<span>Share:</span>
										<ul class="social-icons icon-rounded-circle icon-zoomed list-unstyled list-inline nav justify-content-center"> 
											<li class="pr-2">
												<a target="_blank" onClick="gtag(\'event\', \'share\', { \'event_category\': \'social\', \'event_label\': \'facebook\' })" href="https://www.facebook.com/dialog/feed?app_id=184683071273&amp;link={{rawurlencode(url()->current())}}&amp;picture={{urlencode(asset('uploads/'.$news->image))}}&amp;name={{urlencode($news->name)}}&amp;caption={{urlencode($news->name)}}&amp;description={{rawurlencode(str_limit(strip_tags($news->description),100,''))}}&amp;redirect_uri=http%3A%2F%2Fwww.facebook.com%2F"><i class="fab fa-facebook"></i></a>
											</li>
											<li>
												<a target="_blank" onClick="gtag(\'event\', \'share\', { \'event_category\': \'social\', \'event_label\': \'twitter\' })" href="http://twitter.com/intent/tweet?text={{rawurlencode($news->name)}}%20{{url()->current()}}"><i class="fab fa-twitter"></i></a>
											</li>													
											{{-- <li><a href="#"><i class="fab fa-youtube"></i></a></li> --}}
											{{-- <li><a href="#"><i class="fab fa-linkedin"></i></a></li>  --}}
											{{-- <li><a href="#"><i class="fab fa-google-plus"></i></a></li>  --}}
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="clear"></div>
					<div class="divider divider-center"><i class="icon-circle"></i></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12 bg-warning px-0">
				<div class="bg-wave-down"></div>
				<div class="bg-alpha-pattern4-warning content-news-warning">
					<div class="mx-auto p-5">
						@if($news_lists->count() > 3)
						<div class="main-carousel owl-carousel owl-theme">
							@foreach($news_lists as $news)
							@if(File::exists(public_path('uploads/273x273px_'.$news->image)))
							<div class="item">
								<img src="{{asset('uploads/273x273px_'.$news->image)}}" data-src="{{ asset('uploads/273x273px_'.$news->image) }}" alt="{{ $news->name }} " class="owl-lazy"/>
								<div class="owl-caption animated fadeIn">
									<a href="{{ route('news_event.show', $news->slug) }}" title="{{$news->name}}"><span class="animated fadeInDown">{{ $news->name }}</span></a>
								</div>
							</div>
							@endif
							@endforeach
						</div>  
						@endif
					</div>
				</div>
			</div>
		</div>	
	</div>
</div>
@stop
