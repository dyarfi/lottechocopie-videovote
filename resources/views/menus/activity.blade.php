@extends('layouts.master')

@section('content')
{{-- <div class="main-carousel-alt owl-carousel owl-theme"> --}}
	{{-- @foreach($banners as $banner)
	@if(File::exists(public_path('uploads/1622x800px_'.$banner->image)))
	<div class="item">
		<img src="{{asset('uploads/1622x800px_'.$banner->image)}}" data-src="{{ asset('uploads/1622x800px_'.$banner->image) }}" alt="{{ $banner->name}} " class="owl-lazy"/>
		<div class="owl-caption d-none">
			<a href="{{ url('news_event.show', $banner->url) }}"><span>{{ $banner->name }}</span></a>
		</div>
	</div>
	@endif
	@endforeach --}}
{{-- </div> --}}
<img src="{{asset('images/ACTIVITY-BANNER.jpg')}}" alt="Activity Banner" class="img-fluid d-none d-lg-block"/>
<img src="{{asset('images/grandprize-copy-mobile.jpg')}}" alt="Activity Banner" class="img-fluid d-lg-none d-sm-block"/>
<div class="container-fluid">
	<div class="justify-content-center my-3">
		<div class="container-fluid">
			<div class="card-deck">
				<?php
				$e = $videos->count();
				$c = 1;
				?>
				@foreach ($videos as $video)
					<div class="col-sm-12 col-md-4 pb-5">
						<div class="card mb-5 video-thumb mx-auto" style="background-image: url('{{asset('uploads/512x490px_'.$video->image)}}')">
						<h5 class="text-white uppercase pt-3 header-ams-white">{{str_limit($video->name,20)}}</h5>
							<div class="position-absolute" style="top: 9%;right: 0%;left:0%; text-align:center">
								{{-- <a href="javascript:;" data-rel="{{ $video->url }}" class="btn-play-video mx-auto"></a> --}}
								<a data-fancybox data-src="#hidden-content1" href="#{{$video->slug}}" class="btn-play-video mx-auto"></a>
							</div>
							<div class="position-absolute" style="bottom: -23.5%;right: 0%;left:0%; text-align:center">
							{{-- <a data-fancybox data-src="#hidden-content1" data-width="580" data-height="680" href="#{{$video->slug}}" class="btn btn-warning btn-rounded open-modal">Vote</a> --}}
								<div class="text-center mt-3 mb-2">
									<h6 class="vote_count">
										<i class="fas fa-heart text-ambass"></i> {{ $video->votes->count() }} Votes
									</h6>
								</div>
								<a data-fancybox data-src="#hidden-content1" href="#{{$video->slug}}" class="btn btn-warning btn-rounded open-modal">Vote</a>
							</div>
						</div>
					</div>
				<?php $c++; ?>
				@endforeach
				<div id="hidden-content3" style="display:none">
					<div class="content-popper">
						<div class="justify-content-center">
							<div class="video-content">
								<div class="embed-holder">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe class="embed-responsive-item" allowfullscreen></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="hidden-content1" style="display:none">
				<div class="content-popper">
					<ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
						<?php
						$a = 1;
						?>
						@foreach ($videos as $video)
							<li class="nav-item">
							<a class="nav-link{{$a == 0 ? ' active' :''}}" id="{{$video->slug}}-tab" data-toggle="tab" href="#{{$video->slug}}" role="tab" aria-controls="{{$video->slug}}" aria-selected="true">{{$video->name}}</a>
							</li>
							<?php $a++;?>
						@endforeach
					</ul>
					<div class="tab-content" id="myTabContent">
						<?php
						$a = 1;
						?>
						@foreach ($videos as $video)
						<div class="tab-pane fade show{{$a==0 ? ' active' :''}}" id="{!! $video->slug !!}" role="tabpanel" aria-labelledby="{{$video->slug}}-tab">
							<h2 class="video-head">Video {{$a}} : <span>{!! $video->name !!}</span></h2>							
							<div class="video-content">
								<div class="embed-holder">
									<div class="embed-responsive embed-responsive-16by9">
										<iframe class="embed-responsive-item" src="{{ $video->url }}" allowfullscreen></iframe>
									</div>
								</div>
								<div class="text-right mt-3">
									<h5 class="vote_count">
									<i class="fas fa-heart text-ambass"></i> {{ $video->votes->count() }} Votes
									</h5>
								</div>
								<div>
									{!! $video->description !!}
									@if(isset($user->vote) && $user->vote->attribute)
    								<div class="alert alert-success mb-0" role="alert">
										<span class="font-weight-bold text-center">Terima kasih sudah memvoting</span>
									</div>
									@else
								<div class="text-center"><a href="{{route('video.vote.get',$video->slug)}}" class="btn btn-warning bg-button-alt btn-rounded mt-2 open-vote" data-url="{{$video->slug}}" data-id="part-{{ ($user) ? $user->id : '0'}}" data-video="{{$video->slug}}">{{($user) ? 'Vote' : 'Login to Vote'}}</a></div>
									@endif
	
									<div class="text-center mt-3">
										<a href="{{url('page/terms-and-conditions')}}">Terms and Conditions</a>
									</div>
									
								</div>
							</div>
						</div>
						<?php $a++;?>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="justify-content-center my-3">
		<div class="card-deck">
			<div class="card card-price">
				<img class="card-img-top" src="{{asset('images/img-prize1.jpg')}}">
				<div class="card-body d-none">
					<div class="my-3">
						<h5 class="text-secondary">Prize A</h5>
						<div class="card-text">
							<p class="py-5">Prize A Description</p>
						</div>
					</div>
				</div>
			</div>
			<div class="card card-price">
				<img class="card-img-top" src="{{asset('images/img-prize2.jpg')}}">
				<div class="card-body d-none">
					<div class="my-3">
						<h5 class="text-secondary">Prize B</h5>
						<div class="card-text">
							<p class="py-5">Prize B Description</p>
						</div>
					</div>
				</div>
			</div>
			<div class="card card-price">
				<img class="card-img-top" src="{{asset('images/img-prize3.jpg')}}">
				<div class="card-body d-none">
					<div class="my-3">
						<h5 class="text-secondary">Prize C</h5>
						<div class="card-text">
							<p class="py-5">Prize C Description</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="row">
		<div class="col-md-4">
            <img class="img-fluid ilist-c" data-src="holder.js/633x528?theme=thumb" src="https://lottechocopieindonesia.com/uploads/633x528px_news_98200.jpg" alt="Lotte Choco Pie Goes To School">
            <!--article class="mb-4">
                <figure class="gray imghvr-zoom-in">
                    <img class="img-fluid ilist-c" data-src="holder.js/633x528?theme=thumb" src="https://lottechocopieindonesia.com/uploads/633x528px_news_98200.jpg" alt="Lotte Choco Pie Goes To School">
                    <figcaption class="text-center">
                        <h6 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">Lotte Choco Pie Goes To School</h6>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="https://lottechocopieindonesia.com/news_event/lotte-choco-pie-goes-to-school" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
								Tags : <a href="javascript:;" class="text-warning" title="Development">Development,</a>
								<a href="javascript:;" class="text-warning" title="Design">Design,</a>
								<a href="javascript:;" class="text-warning" title="Creative">Creative</a>
							</div>
							<div>Category : <a href="#" title="Web Development" class="text-warning">Web Development</a></div>
						</div>
                    </figcaption>
                </figure>
                <div class="text-center py-2">
                    <p>
                    Sekolah-sekolah yang memilih video versi homework akan diundi oleh Lotte Choco Pie untuk didatangi oleh Lotte Choco Pie Troop. Sang Troop leader adalah seorang selebriti yang akan berbagi ce ...
                    </p>
                </div>
            </article-->
            <div class="text-center py-2">
				<p>Mom &amp; si Kecil yang memilih video versi homework akan diundi oleh Lotte Choco Pie untuk menghadiri kelas yang membahas pentingnya momen premium serta diajak mendonasikan perlengkapan sekolah.
				</p>
			</div>
        </div>
		<div class="col-md-4">
            <img class="img-fluid ilist-c" data-src="holder.js/633x528?theme=thumb" src="https://lottechocopieindonesia.com/uploads/633x528px_news_61386.jpg" alt="Sehari Menjadi Manajer Supermarket Bersama Lotte Choco Pie">
            <!--article class="mb-4">
                <figure class="gray imghvr-zoom-in">
                    <img class="img-fluid ilist-c" data-src="holder.js/633x528?theme=thumb" src="https://lottechocopieindonesia.com/uploads/633x528px_news_61386.jpg" alt="Sehari Menjadi Manajer Supermarket Bersama Lotte Choco Pie">
                    <figcaption class="text-center">
                        <h6 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">Sehari Menjadi Manajer Supermarket Bersama Lotte Choco Pie</h6>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="https://lottechocopieindonesia.com/news_event/sehari-menjadi-manajer-supermarket-bersama-lotte-choco-pie" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            	<div class="mx-auto">
									Tags : <a href="javascript:;" class="text-warning" title="Design">Design,</a>
									<a href="javascript:;" class="text-warning" title="Development">Development</a>
								</div>
								<div>Category : <a href="#" title="Web Development" class="text-warning">Web Development</a></div>
							</div>
                    </figcaption>
                </figure>
                <div class="text-center py-2">
                    <p>
                    Lotte Choco Pie akan mengundi Mom &amp; si Kecil yang memilih video versi shopping. Pemenangnya akan mengikuti aktivitas sehari menjadi staff supermarket dan menghabiskan momen premium bersa ...
                    </p>
                </div>
            </article-->
            <div class="text-center py-2"><p>Lotte Choco Pie akan mengundi Mom &amp; si Kecil yang memilih video versi shopping. Pemenangnya akan mengikuti aktivitas sehari menjadi staff supermarket dan menghabiskan momen premium bersama.</p></div>
        </div>
		<div class="col-md-4">
            <img class="img-fluid ilist-c" data-src="holder.js/633x528?theme=thumb" src="https://lottechocopieindonesia.com/uploads/633x528px_news_78174.jpg" alt="Ucapkan Terima Kasih dengan Lotte Choco Pie">
            <!--article class="mb-4">
                <figure class="gray imghvr-zoom-in">
                    <img class="img-fluid ilist-c" data-src="holder.js/633x528?theme=thumb" src="https://lottechocopieindonesia.com/uploads/633x528px_news_78174.jpg" alt="Ucapkan Terima Kasih dengan Lotte Choco Pie">
                    <figcaption class="text-center">
                        <h6 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">Ucapkan Terima Kasih dengan Lotte Choco Pie</h6>
                        <div class="ih-zoom-in ih-delay-md">
                            <a href="https://lottechocopieindonesia.com/news_event/ucapkan-terima-kasih-dengan-lotte-choco-pie" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
                            <div class="mx-auto">
								Tags : <a href="javascript:;" class="text-warning" title="Design">Design</a>
							</div>
							<div>Category : <a href="#" title="Web Design" class="text-warning">Web Design</a></div>
						</div>
                    </figcaption>
                </figure>
                <div class="text-center py-2">
                    <p>
                    Para Mom yang memilih video versi greeting akan diundang oleh Lotte Choco Pie untuk mengikuti sesi berbagi bersama para Mom lainnya dan selebriti. Acara ini akan membahas topik mengenai perk ...
                    </p>
                </div>
            </article-->
            <div class="text-center py-2"><p>Mom yang memilih video versi greeting akan diundang oleh Lotte Choco Pie untuk mengikuti sharing session premium yang membahas topik mengenai perkembangan &amp; cara membesarkan si Kecil.</p></div>
        </div>
	</div>

	<div class="justify-content-between my-3" id="ambassador">
		<div class="card-group">
			<div class="row">
				<?php
				$d = count($ambassadors);
				$v = 1;
				?>
				@foreach ($ambassadors as $ambassador)
					<div class="col-sm-6 col-md-4 mb-4 mx-auto">
						<div class="card bg-light">
							<div class="row">
								<div class="card-header d-none">
									<span class="card-title text-secondary font-weight-bold">{{ $ambassador->name }}</span>
								</div>
								<div class="card-body mx-auto">
									@if($ambassador->image && File::exists(public_path('uploads/'.$ambassador->image)))
									<div class="text-center">
										<a data-fancybox data-src="#hidden-content2" href="javascript:;">
											<img class="img-fluid rounded-circle ilist" src="{{ asset('uploads/'.$ambassador->image) }}" alt="{{ $ambassador->name }}">
										</a>
									</div>
									@endif
									<div class="card-text text-center text-ambass" <?php echo ($ambassador->video) ? 'data-url="'.$ambassador->video->url.'" data-video="'.$ambassador->video->name.'"':''; ?>>
										<h6 class="font-weight-bold mt-3">{{ $ambassador->name }}</h6>
										<p class="text-ambass">{!! strip_tags($ambassador->description) !!}</p>
									</div>
								</div>
								<div class="card-footer d-none">Posted : {{ $ambassador->created_at }}</div>
							</div>
						</div>
					</div>
					<?php
					$v++;
					?>
				@endforeach
			</div>
		</div>
		<div id="hidden-content2" style="display:none">
			<div class="content-popper">
				<h4 class="video-head-alt">
					<img src="" data-src="holder.js/50x50/thumb" class="rounded-circle"> <span class="text-ambass text-uppercase"></span>
				</h4>
				{{-- <img class="video-head-img w-100 img-fluid py-3 px-1" src=""> --}}
				<div class="justify-content-center">
					<div class="video-content">
						<div class="embed-holder">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
				<div>
					<h2 class="my-2 video-head-content"> </h2>
					<div class="pb-4">
						<p class="video-head-desc"></p>
						<div class="col-12 mt-3">
							<div class="float-right">
								<span>Share:</span>
								<ul class="social-icons icon-rounded-circle icon-zoomed list-unstyled list-inline nav justify-content-center">
									{{-- <li class="pr-2"><a href="#"><i class="fab fa-facebook"></i></a></li>
									<li><a href="#"><i class="fab fa-twitter"></i></a></li> --}}
									<li class="pr-2">
									<a target="_blank" onClick="gtag(\'event\', \'share\', { \'event_category\': \'social\', \'event_label\': \'facebook\' })" href="https://www.facebook.com/dialog/feed?app_id=184683071273&amp;link={{url()->current()}}&amp;picture={{rawurlencode(asset('images/img-banner-activity.jpg'))}}&amp;name=Terima%20Kasih&amp;caption=%20&amp;description={{urlencode('Lotte Choco Pie mengapresiasi proses tumbuh kembang si Kecil, maka dari itu melalui #PremiumMomentstogether kami mengajak Mom berbagi cerita pertumbuhan si Kecil dengan memilih cerita yang sesuai pengalaman. Grand prize trip ke Jepang &amp; hadiah menarik lain bisa dimenangkan!')}}&amp;redirect_uri=http%3A%2F%2Fwww.facebook.com%2F"><i class="fab fa-facebook"></i></a>
									</li>
									<li>
										<a target="_blank" onClick="gtag(\'event\', \'share\', { \'event_category\': \'social\', \'event_label\': \'twitter\' })" href="http://twitter.com/intent/tweet?text={{urlencode('Lotte Choco Pie mengapresiasi proses tumbuh kembang si Kecil, maka dari itu melalui #PremiumMomentstogether kami mengajak Mom berbagi cerita pertumbuhan si Kecil dengan memilih cerita yang sesuai pengalaman. Grand prize trip ke Jepang dan hadiah menarik lain bisa dimenangkan!')}}%20{{url()->current()}}"><i class="fab fa-twitter"></i></a>
									</li>	
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
