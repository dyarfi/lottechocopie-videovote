@extends('layouts.master')

@section('content')				
<div class="clearfix">
	<div class="col bg-brown p-0">
		<div class="mx-auto text-center py-4">
			<h1 class="font-weight-bold text-center text-white text-uppercase text-shadow-black">{{ $menu->name }}</h1>
			<span class="d-none">{!!$menu->description !!}</span>
		</div>
		<div class="row-fluid position-relative">
			<div class="position-absolute m-4 mr-lg-5 mb-lg-5 news-excerpt">
				<div class="row-fluid">
					<div class="col-sm-12 col-md-12 col-lg-6 bg-alpha-dark text-white float-right box-shadow">
						<h4 class="content-head-alt text-shadow-black">{{$news->name}}</h4>
						<div class="content my-3">
							{!! str_limit(strip_tags($news->excerpt),100,' ...') !!}
						</div>
						<a class="btn btn-outline-light btn-sm btn-rounded text-uppercase" title="More Info on {{$news->name}}" href="{{ route('news_event.show',$news->slug) }}">More Info</a>
					</div>
				</div>
			</div>
			<img src="{{asset('uploads/'.$news->image) }}" class="img-fluid" alt="{{$news->name}}">
		</div>
	</div>
	<div class="position-relative">
		<div class="bg-wave-up mt-m-26"></div>
			<div class="container bg-white">
				<div id="entry-listing">
					<div class="row">
						@if($news_list)
						@foreach($news_list as $news)
						@if(File::exists(public_path('uploads/632x300px_'.$news->image)))
						<div class="entry col-md-6">		
							<div class="px-0 px-sm-3 pb-3">
								<a href="{{route('news_event.show',$news->slug)}}" title="{{$news->name}}">
									<img src="{{ asset('uploads/632x300px_'.$news->image) }}" data-src="holder.js/632x300?theme=thumb" alt="{{$news->name}}" class="img-fluid rounded ilist-c">
								</a>
								<div class="div-abs">
									<a class="text-warning" href="{{route('news_event.show',$news->slug)}}" title="{{$news->name}}">
										{{str_limit($news->name,86,'..')}}
									</a>	
									<div class="clearfix text-white small mt-0 mt-sm-2">{{$news->created_at}}</div>
								</div>			
							</div>
							<?php
							/*
							<article class="mb-4">
								<figure class="gray imghvr-zoom-in">
									<img class="img-fluid ilist-c" data-src="holder.js/632x300?theme=thumb" src="{{ asset('uploads/632x300px_'.$news->image) }}" alt="{{ $news->name }}">
									<figcaption class="text-center">
										<h5 class="ih-zoom-in ih-delay-xs font-weight-bold hbb-gray">{{ $news->name }}</h5>
										<p class="ih-zoom-in ih-delay-sm">{{ $news->name }}</p>
										<div class="ih-zoom-in ih-delay-md">
											<a href="{{ route('news_event.show',$news->slug) }}" class="btn btn-outline-light btn-rounded btn-sm text-uppercase my-2"><i class="fas fa-link"></i> More</a>
											<div class="mx-auto">											 
												<?php
												$i=1;
												$t=count($news->tags);
												if ($t > 0) {
													echo 'Tags :';
													foreach ($news->tags as $tags) { ?>
														<a href="javascript:;" class="text-warning" title="{{ $tags->name }}">{{ $tags->name }}@if ($i != $t),@endif</a>
														<?php $i++;
													}
												}
												?>
											</div>
											@if( count($news->category) > 0 )
											<div>Category : <a href="#" title="{{ $news->category->name }}" class="text-warning">{{ $news->category->name }}</a></div>
											@endif
										</div>
									</figcaption>
								</figure>						
							</article> 
							*/
							?>
						</div>
						@endif
						@endforeach
						@endif
					</div>
				</div>
				{!! $news_list->render() !!}					
			</div>
		</div>
	</div>
</div>		
@stop
