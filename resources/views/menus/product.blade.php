@extends('layouts.master')

@section('content')

<div class="container p-0 bg-shadow">
	{{-- <div class="main-carousel-alt owl-carousel owl-theme">
		@foreach($banners as $banner)
		@if(File::exists(public_path('uploads/1622x800px_'.$banner->image)))
		<div class="item">
			<img src="{{asset('uploads/1622x800px_'.$banner->image)}}" data-src="{{ asset('uploads/1622x800px_'.$banner->image) }}" alt="{{ $banner->name}} " class="owl-lazy"/>
			<div class="owl-caption d-none">
				<a href="{{ url('news_event.show', $banner->url) }}"><span>{{ $banner->name }}</span></a>
			</div>
		</div>
		@endif
		@endforeach
	</div> --}}
	<img src="{{asset('images/product-banner.jpg')}}" class="img-fluid">
	<div class="position-relative">
		<div class="bg-wave-brown-up mt-m-26"></div>
		<div class="bg-brown">
			<div class="bg-alpha-pattern1-warning">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 col-md-5">
							<img src="{{asset('images/product-one.png')}}" class="img-fluid" data-src="holder.js/200x300?theme=thumb" alt="Image One">							
						</div>
						<div class="col-sm-12 col-md-7 p-5">                
							<h1 class="main-head-alt">
								Lotte Choco Pie
							</h1>
							<div class="main-content-alt">
								Cemilan premium dengan perpaduan yang sempurna antara lembutnya keik vanilla, kenyalnya marshmallow serta tebalnya lapisan luar yang kaya dengan cokelat premium yang terjamin kualitas dan kelezatannya.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-custom-product">
			<div class="bg-alpha-pattern1-warning">
				<div class="bg-wave-brown-down"></div>
				<div class="pb-4 mt-m-1-5">
					<div class="mx-auto pb-3 text-center">
						<div class="container p-0 p-sm-2">
							<div class="position-relative">
							<img src="{{asset('images/img-product-content.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Image One" class="img-fluid ilist-c">
							<div class="row m-0 p-0">
								{{-- <img src="{{asset('images/img-product-content-full.png')}}" class="img-fluid ilist-c" data-src="holder.js/200x300?theme=thumb" alt="Image One"> --}}
								<div class="col order-1 m-0 p-0 img-softcake">
									<img src="{{asset('images/img-pf-softcake.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Softcake" class="img-fluid ilist-c">
								</div>
								<div class="col order-2 m-0 p-0 img-marsmellow">
									<img src="{{asset('images/img-pf-marsmellow.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Marsmellow" class="img-fluid ilist-c">
								</div>
								<div class="col order-3 m-0 p-0 img-chocolate">
									<img src="{{asset('images/img-pf-chocolate.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Chocolate" class="img-fluid ilist-c">
								</div>
							</div>
							{{-- <div class="row">
								<div class="col-sm order-1">
									<div class="position-relative">
										 <h4><img src="{{asset('images/img-pf-softcake.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Softcake" class="ilist-c"> Softcake</h4>
										<!--svg width="300" height="600" viewBox="0 0 300 600" id="theSVG">									
	<polyline id="line" stroke="#A5CE39" stroke-miterlimit="10" fill="none" style="stroke-width:2;stroke: yellow;stroke-dasharray: 4;" points="80.5,601.167 80.5,241.834 303.167,241.834 "/>	
											<circle id="circle" r="10" cx="0" cy="0" fill="tomato" />
										</svg-->
									</div>
								</div>
								<div class="col-sm order-2">
									 <h4><img src="{{asset('images/img-pf-marsmellow.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Marsmellow" class="ilist-c"> Marsmellow</h4>
								</div>
								<div class="col-sm order-3">
									 <h4><img src="{{asset('images/img-pf-chocolate.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Chocolate" class="ilist-c"> Chocolate</h4>
								</div>
							</div> --}}
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-white mb-5">
			<div class="pt-4">
				<div class="text-center">
					<h2 class="mt-4 main-head header-ams">Our Variants</h2>
				</div>
				<div class="container pb-5 product-lists">
					<div class="mx-auto">
						<div class="row justify-content-center">					
							<div class="col-sm-12 col-md-3">
								<div class="row clearfix">
									<div class="img-border">
										<span class="helper"></span>
										{{-- <a data-fancybox="gallery" data-caption="LOTTE CHOCO PIE Marshmallow 1 Pcs Per Pack" href="{{asset('images/product1.png')}}"><img src="{{asset('images/product1.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Image One"></a> --}}
										{{-- <a href="#{{asset('images/product1.png')}}"> --}}
											<img src="{{asset('images/product1.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Image One">
										{{-- </a> --}}
										<span class="helper"></span>
									</div>
									<div class="mx-auto mt-4 p-2">
										LOTTE CHOCO PIE Marshmallow 1 Pcs Per Pack
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-3">
								<div class="row clearfix">
									<div class="img-border">
										<span class="helper"></span>								
										{{-- <a data-fancybox="gallery" data-caption="LOTTE CHOCO PIE Marshmallow 1 Pcs Per Pack" href="{{asset('images/product2.png')}}"><img class="ilist-c" src="{{asset('images/product2.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Image One"></a> --}}
										{{-- <a href="#{{asset('images/product2.png')}}"> --}}
											<img class="ilist-c" src="{{asset('images/product2.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Image One">
										{{-- </a> --}}
										<span class="helper"></span>
									</div> 
									<div class="mx-auto mt-4 p-2">
										LOTTE CHOCO PIE Marshmallow 2 Pcs Per Pack
									</div>
								</div>
							</div>
							<div class="col-sm-12 col-md-3">
								<div class="row clearfix">
									<div class="img-border">
										<span class="helper"></span>								
										{{-- <a data-fancybox="gallery" data-caption="LOTTE CHOCO PIE Marshmallow 1 Pcs Per Pack" href="{{asset('images/product3.png')}}"><img src="{{asset('images/product3.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Image One"></a> --}}
										{{-- <a href="#{{asset('images/product3.png')}}"> --}}
											<img src="{{asset('images/product3.png')}}" data-src="holder.js/200x300?theme=thumb" alt="Image One">
										{{-- </a> --}}
										<span class="helper"></span>
									</div>
									<div class="mx-auto mt-4 p-2">
										LOTTE CHOCO PIE Marshmallow 6 Pcs Per Pack
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	
@stop
