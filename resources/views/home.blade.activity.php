<?php
// This is supposed to be working with symlik in storage/app/public into public/upload
// <img src="{{ asset('upload/27257.jpg') }}"/>
?>
@extends('layouts.master')

@section('content')

<div id="carouselExampleIndicators" class="carousel slide mt-5" data-ride="carousel">
	<ol class="carousel-indicators">
		<?php $i = 0;?>
		@foreach ($banners as $banner)
			<li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i;?>" class="<?php echo $i == 1 ? 'active' : '';?>"></li>
		<?php $i++; ?>
		@endforeach
	</ol>
	<div class="carousel-inner">
		<?php $j = 0;?>
		@foreach ($banners as $banner)
			@if($banner->image && File::exists(public_path('uploads/'.$banner->image)))
			<div class="carousel-item <?php echo $j == 1 ? 'active':'';?>">
				<img src="{{ asset('uploads/'.$banner->image) }}" alt="{{ $banner->name }}">
				<div class="carousel-caption d-none d-md-block">
					<h2 data-caption-animate="fadeInUp">{{ $banner->name }}</h2>
					<p data-caption-animate="fadeInUp" data-caption-delay="180">
						{{ str_limit(strip_tags($banner->description),200,'') }}
					</p>
				</div>
			</div>
			@endif
		<?php $j++;?>	
		@endforeach
	</div>			
	@if(sizeof($banners) > 1)
		<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="carousel-control-prev-icon" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="carousel-control-next-icon" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>
	@endif			
</div>
<div class="text-center my-4">
	<h1 class="display-4 my-0">
		Videos Campaign
	</h1>
	<h4 class="text-muted">Choose your favorite video</h4>
</div>
<div class="justify-content-center my-3">
	<div class="card-deck">
		<div class="row">
			@foreach ($videos as $video)
			<div class="col-sm-4 mb-4">
				<div class="card mb-3">
					<div class="card-body">
						<h5 class="card-title text-secondary">{{ str_limit($video->name,52,' ...') }}</h5>
						<div>{!! $video->embed !!}</div>
						<div class="card-text text-muted my-3">{!! str_limit($video->description,100,' ...') !!}</div>
						<?php 
						$url = str_replace('watch?v=','embed/',$video->url) . '?autoplay=1';
						?>
						<a class="btn btn-primary btn-md btn-video btn-play-video" href="javascript:;" data-rel="{{ $url }}">
						Vote
						</a>					
					</div>
					<div class="card-footer">
						Posted : {{ $video->created_at }}
					</div>
				</div>
			</div>	
			@endforeach
		</div>
	</div>
</div>
<div class="text-center my-4">
	<h1 class="display-4 my-0">
		Commitments
	</h1>
	<h4 class="text-muted">PR Events</h4>
</div>	
<div class="justify-content-center my-3">
	<div class="card-deck">
		@foreach ($events as $event)
		<div class="card text-center">
			<div class="card-header">						
				<span class="font-weight-bold text-secondary">{{ $event->name }}</span>
			</div>
			<div class="card-body">
				<div class="my-3">
					<div class="card-text">
						<p class="py-5">{!! $event->description !!}</p>
					</div>
				</div>
			</div>
			<div class="card-footer">
				Event Date : {{ $event->event_date }}
			</div>
		</div>
		@endforeach
	</div>
</div>
<div class="text-center my-4">
	<h1 class="display-4 my-0">
		Prizes
	</h1>
	<h4 class="text-muted">Common Prizes</h4>
</div>	
<div class="justify-content-center my-3">
	<div class="card-deck">
		<div class="card">
			<div class="card-body">
				<div class="my-3">
					<h5 class="text-secondary">Prize A</h5>
					<div class="card-text">
						<p class="py-5">Prize A Description</p>
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<div class="my-3">
					<h5 class="text-secondary">Prize B</h5>
					<div class="card-text">
						<p class="py-5">Prize B Description</p>								
					</div>
				</div>
			</div>
		</div>
		<div class="card">
			<div class="card-body">
				<div class="my-3">
					<h5 class="text-secondary">Prize C</h5>
					<div class="card-text">
						<p class="py-5">Prize C Description</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="text-center my-4">
	<h1 class="display-4 my-0">
		Ambassadors
	</h1>
	<h4 class="text-muted">Influencer / Blogger</h4>
</div>
<div class="justify-content-between my-3">
	<div class="card-deck">
		<div class="row">
		@foreach ($ambassadors as $ambassador)				
			<div class="col-sm-4 mb-4">
				<div class="card">						
					<div class="card-header">
						<span class="card-title text-secondary font-weight-bold">{{ $ambassador->name }}</span>
					</div>
					@if($ambassador->image && File::exists(public_path('uploads/'.$ambassador->image)))
						<img class="card-img-top img-fluid" src="{{ asset('uploads/'.$ambassador->image) }}" alt="{{ $ambassador->name }}">
					@endif	
					<div class="card-body">
						<div class="card-text">
							<p class="text-muted">{!! $ambassador->description !!}</p>
						</div>									
					</div>
					<div class="card-footer">
						Posted : {{ $ambassador->created_at }}
					</div>
				</div>
			</div>
		@endforeach
		</div>		
	</div>
</div>

@stop
