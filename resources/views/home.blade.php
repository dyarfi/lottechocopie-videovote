<?php
// This is supposed to be working with symlik in storage/app/public into public/upload
// <img src="{{ asset('upload/27257.jpg') }}"/>
?>
@extends('layouts.master')

@section('content')

<div class="container p-0 bg-shadow">
	<div class="main-carousel-alt owl-carousel owl-theme">
		@foreach($banners as $banner)
			@if(File::exists(public_path('uploads/1622x800px_'.$banner->image)))
			{{-- <div class="item"> --}}
				<a href="{{ $banner->attributes }}" class="item">
					<img src="{{asset('uploads/1622x800px_'.$banner->image)}}" data-src="{{ asset('uploads/1622x800px_'.$banner->image) }}" alt="{{ $banner->name}}" class="owl-lazy"/>
				</a>
			{{-- </div> --}}
			@endif
		@endforeach
	</div>  
	<div class="position-relative">
		<div class="bg-wave-up mt-m-26"></div>
		<div class="bg-white">
			<div class="row">
				<div class="col-sm-12 col-md-5 py-5">
					<div class="container px-5 pt-4">
						<img src="{{asset('images/product-home.png')}}" class="img-fluid text-center" data-src="holder.js/200x300?theme=thumb" alt="Image One">					
						<h1 class="font-weight-bold text-warning">Lotte Choco Pie</h1>
						<p>Cemilan premium dengan perpaduan yang sempurna antara lembutnya keik vanilla, kenyalnya marshmallow serta tebalnya lapisan luar yang kaya dengan cokelat premium yang terjamin kualitas dan kelezatannya.</p>
						<a href="{{route('product')}}" class="btn bg-button-alt-ff btn-rounded">View More</a>
					</div>
				</div>
				<div class="col-sm-12 col-md-7 py-5 px-4 px-sm-4">
					<div class="px-2 px-sm-4 pr-md-5 pt-0 pt-sm-0 pt-md-4">
						<div class="embed-holder">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tMaPUkJIzbg?rel=0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="bg-custom">
			<div class="bg-alpha-pattern1-warning">
				<div class="bg-wave-down"></div>
				<div class="p-3 p-sm-5">
					<div class="text-center">
						<h2 class="p-0 main-head header-ams">News &amp; Event</h2>
					</div>
					<div class="pb-4">
						<div class="container">
							<div class="row">
								@foreach ($news_list as $news)
								@if(File::exists(public_path('uploads/633x528px_'.$news->image)))
								<div class="col-12 col-sm-12 col-md-6">
									<div class="px-0 px-sm-3 pb-3">
										<span class="span-abs">News</span>
										<a href="{{route('news_event.show',$news->slug)}}" title="{{$news->name}}">
											<img src="{{ asset('uploads/633x528px_'.$news->image) }}" data-src="holder.js/633x528?theme=thumb" alt="{{$news->name}}" class="img-fluid rounded ilist-c">
										</a>
										<div class="div-abs">
											<a class="text-warning" href="{{route('news_event.show',$news->slug)}}" title="{{$news->name}}">
												{{str_limit($news->name,86,'...')}}
											</a>	
											<div class="clearfix text-white small mt-2">{{$news->created_at}}</div>
										</div>
									</div>
								</div>
								@endif
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop
