// jQuery Init
const $ = jQuery;
const $body = $('body');

// Promise based ajax
import axios from "axios";
// axios default settings
axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN' : document.querySelector('meta[name="csrf-token"]').getAttribute('content')
};
// Index js for React components
// import Index from './index.js';

var LOTCHOC = LOTCHOC || {};

var LOTCHOC = {
    // Initialize 
    init : function() {
        // React modules Initialize
        // Index.init();

        // Layout Initialize
        LOTCHOC.responsiveClasses();
        LOTCHOC.searchOverlay();
        LOTCHOC.owlCarousel();
        LOTCHOC.iasContent();
        LOTCHOC.fancyBox();
        // LOTCHOC.tweenMax();
        LOTCHOC.holderThumb();
        LOTCHOC.accountFormHandler();
        LOTCHOC.loginHandler();
        LOTCHOC.registerHandler();
        // Modals
        LOTCHOC.videoModal();
        LOTCHOC.thanksModal();
        LOTCHOC.activityModal();
        LOTCHOC.voteModal();
        LOTCHOC.kolModals();
        LOTCHOC.videosModal();
    },

    // jRespond Init
    responsiveClasses : function() {
        if( typeof jRespond === 'undefined' ) {
            console.log('responsiveClasses: jQuery jRespond not Defined.');
            return true;
        }
        var jRes = jRespond([
            {
            label: 'smallest',
            enter: 0,
            exit: 479
            },{
            label: 'handheld',
            enter: 480,
            exit: 767
            },{
            label: 'tablet',
            enter: 768,
            exit: 991
            },{
            label: 'laptop',
            enter: 992,
            exit: 1199
            },{
            label: 'desktop',
            enter: 1200,
            exit: 10000
            }
        ]);
        jRes.addFunc([
            {
            breakpoint: 'desktop',
            enter: function() { $body.addClass('device-lg'); },
            exit: function() { $body.removeClass('device-lg'); }
            },{
            breakpoint: 'laptop',
            enter: function() { $body.addClass('device-md'); },
            exit: function() { $body.removeClass('device-md'); }
            },{
            breakpoint: 'tablet',
            enter: function() { $body.addClass('device-sm'); },
            exit: function() { $body.removeClass('device-sm'); }
            },{
            breakpoint: 'handheld',
            enter: function() { $body.addClass('device-xs'); },
            exit: function() { $body.removeClass('device-xs'); }
            },{
            breakpoint: 'smallest',
            enter: function() { $body.addClass('device-xxs'); },
            exit: function() { $body.removeClass('device-xxs'); }
            }
        ]);
    },
    // Search overlay handler
    searchOverlay : function () {
        // Trigger button
        $('#trigger-overlay').on('click',function() {
            $('#overlay-search').toggleClass('open');
            // Form input handler
            $('#form-search input[type=text]').focus();
        });
        // Close button
        $('.overlay-close').on('click',function() {
            $('#overlay-search').removeClass('open');
        });
    },
    // Bootstrap Modal
    videoModal : function() {
        // Play video click handler        
        // $('.btn-play-video').click(function () {
        //     var src = $(this).data('rel') + '?autoplay=1';
        //     console.log(src);
        //     $('#hidden-content3').find('iframe.embed-responsive-item').attr('src',src);
        // });

        /*
        $('.btn-play-video').click(function () {
            var src = $(this).data('rel');
            // var title = $(this).html();
            var content = $(this).find('span.hide').html();
            $('#BootModal').modal('show');
            $('#BootModal iframe').attr('src', src+'?autoplay=1');
            $('#BootModal .content').text(content);
            return false;
        });
        
        // Close button click
        $('#BootModal button, #BootModal .modal-header button.close').click(function () {
            $('#BootModal iframe').removeAttr('src');
        });
        // Modal on hide handler
        $('#BootModal').on('hide.bs.modal', function (e) {
            $('#BootModal iframe').removeAttr('src');
        });
        $('#BootModal').on('hidden.bs.modal', function (e) {
            $('#BootModal iframe').removeAttr('src');
        });
        // $('#BootModal').on('show.bs.modal', function (e) {
            // $('#BootModal iframe').removeAttr('src');
        // });
        // $('#BootModal').on('shown.bs.modal', function (e) {
            // $('#BootModal iframe').removeAttr('src');
        // });
        */
       
    },
    // Bootstrap Modal
    thanksModal : function() {
        // Play video click handler
        /** 
        $('.btn-play-video').click(function () {
            var src = $(this).data('rel');
            // var title = $(this).html();
            var content = $(this).find('span.hide').html();
            $('#BootModal').modal('show');
            // $('#BootModal .modal-title').html('');
            // $('#BootModal .modal-title').html(title);
            $('#BootModal iframe').attr('src', src);
            $('#BootModal .content').text(content);
            return false;
        });
        // Close button click
        $('#BootModal button').click(function () {
            $('#BootModal iframe').removeAttr('src');
        });
        // Modal on hide handler
        $('#BootModal').on('hide.bs.modal', function (e) {
            $('#BootModal iframe').removeAttr('src');
        });
        */
    },
    // OwlCarousel
    owlCarousel : function() {
        var owl1 = $('.partnership-carousel').owlCarousel({
            animateOut:'fadeOut',
            animateIn:'fadeIn',
            items:6,
            autoplay:1,
            loop:1,
            margin:30,
            lazyLoad:true,
            stagePadding:0,
            smartSpeed:350,
            responsive : {
                // breakpoint from 0 up
                0 : {
                    items:2
                },
                // breakpoint from 480 up
                480 : {
                    items:3
                },
                // breakpoint from 768 up
                768 : {
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });
        var owl2 = $('.rates-carousel').owlCarousel({
            animateOut:'fadeOut',
            animateIn:'fadeIn',
            items:6,
            autoplay:1,
            loop:1,
            margin:30,
            lazyLoad:true,
            stagePadding:0,
            smartSpeed:350,
            responsive : {
                // breakpoint from 0 up
                0 : {
                    items:1
                },
                // breakpoint from 480 up
                480 : {
                    items:3
                },
                // breakpoint from 768 up
                768 : {
                    items:4
                },
                1000:{
                    items:6
                }
            }
        });
        var owl = $('.main-carousel').owlCarousel({
            animateOut:'fadeOut',
            animateIn:'fadeIn',
            items:3,
            autoplay:1,
            loop:1,
            nav:true,
            margin:25,
            navText:['<i class="fas fa-chevron-left fa-3x"></i>','<i class="fas fa-chevron-right fa-3x"></i>'],
            lazyLoad:true,
            stagePadding:0,
            smartSpeed:490,
            dots:false,
            responsive : {
                // breakpoint from 0 up
                240 : {
                    items:1
                },
                // breakpoint from 480 up
                480 : {
                    items:1
                },
                // breakpoint from 768 up
                768 : {
                    items:3
                },
                1000:{
                    items:3
                }
            }
        });
                    
        owl.on('changed.owl.carousel', function(event) {
            /*
            var obj = event.target;
            // Item 
            var item = $(obj).find('.owl-caption');
            // Item Head
            var itemHead = item.find('h1,h2,h3,h4');
            // Item Content				
            var itemContent = item.find('p');			
            // Current Owl slide item				
            item.eq(event.item.index)
            .removeClass('d-none')
            .addClass('animated fadeIn');
            // Current Owl slide Heading 
            itemHead.eq(event.item.index)
            .removeClass('d-none')
            .addClass('animated fadeInDown');
            // Current Owl slide Content
            itemContent.eq(event.item.index)
            .removeClass('d-none')
            .addClass('animated fadeInUp');
            */
        });
        owl.on('change.owl.carousel', function(event) {
            /*
            var obj = event.target;
            // Item 
            var item = $(obj).find('.owl-caption').addClass('d-none');
            // Item Head
            var itemHead = item.find('.owl-caption > h1,h2,h3,h4').addClass('d-none');
            // Item Content				
            var itemContent = item.find('.owl-caption > p').addClass('d-none');
            */
        });
    
        var owlalt = $('.main-carousel-alt').owlCarousel({
            animateOut:'fadeOut',
            animateIn:'fadeIn',
            items:1,
            autoplay:1,
            loop:1,
            margin:30,
            //lazyLoad:true,
            stagePadding:0,
            smartSpeed:250,
            dots:false,
        });
                    
        owlalt.on('changed.owl.carousel', function(event) {
            /*
            var obj = event.target;
            // Item 
            var item = $(obj).find('.owl-caption');
            // Item Head
            var itemHead = item.find('h1,h2,h3,h4');
            // Item Content				
            var itemContent = item.find('p');			
            // Current Owl slide item				
            item.eq(event.item.index)
            .removeClass('d-none')
            .addClass('animated fadeIn');
            // Current Owl slide Heading 
            itemHead.eq(event.item.index)
            .removeClass('d-none')
            .addClass('animated fadeInDown');
            // Current Owl slide Content
            itemContent.eq(event.item.index)
            .removeClass('d-none')
            .addClass('animated fadeInUp');
            */
        });
        owlalt.on('change.owl.carousel', function(event) {
            /*
            var obj = event.target;
            // Item 
            var item = $(obj).find('.owl-caption').addClass('d-none');
            // Item Head
            var itemHead = item.find('.owl-caption > h1,h2,h3,h4').addClass('d-none');
            // Item Content				
            var itemContent = item.find('.owl-caption > p').addClass('d-none');
            */
        });
    },
    // IAS Infinite ajax scroll
    iasContent:function () {
        var ias = $.ias({
            container   : '#entry-listing',
            item        : '.entry',
            pagination  : '.pagination',
            next        : 'a[rel="next"]',
            //loader      : '<div class="container waypoint-handler"><div class="spinner scaleout"></div></div>',
            delay       : 1000,
            history     : false,
            negativeMargin : 100,
            //debug : true,
            //dataType : 'html',
            //maxPage : 1,
            onRenderComplete: function(items) {
                // Rebuild jquery binding on widget
                //SEMICOLON.widget.accordions();
                //SEMICOLON.widget.linkScroll();
                // Rebuild jquery binding on initialize
                //SEMICOLON.initialize.pageContent();
                  //var $newElems = jQuery(items).addClass("newItem");
                  //$newElems.hide().imagesLoaded(function(){
                    //jQuery(this).show();
                    //jQuery('#infscr-loading').fadeOut('normal');
                    //jQuery("#entry-listing").isotope('appended', $newElems );
                  //});
            }
        });
        ias.extension(new IASSpinnerExtension({html:'<div class="container py-4"><div class="spinner double-bounce"><div class="double-bounce1"></div><div class="double-bounce2"></div></div></div>'}));
        ias.extension(new IASTriggerExtension({
            text: '<span class="text-white text-uppercase">More</span>',
            html: '<div class="ias-trigger ias-trigger-next mx-auto"><a class="btn btn-outline-warning bg-warning btn-rounded">{text}</a></div>'
        }));
        ias.extension(new IASNoneLeftExtension({
            text: '',
            html: '<div class="ias-noneleft mx-auto"><h5 class="text-center text-warning font-weight-bold">{text}</h5></div>'
        }));
    },
    // Fancybox
    fancyBox:function () {
        //$('[data-fancybox="gallery"]').fancybox({
            // Options will go here
        // });
        $("a[data-fancybox].nav-link").fancybox({
            // Custom CSS class for layout
            onInit:function () {
                // Init login first
                $('.login-handler').show();
                $('.register-handler').hide();
            }
        });
    },
    // TweenMax
    //tweenMax : function() {
        /*
        var path = MorphSVGPlugin.pathDataToBezier("#line");
        TweenMax.to("#circle", 5, {bezier:{values:path, type:"cubic"}, ease:Linear.easeNone, repeat:-1});
        */
    //},
    // Holderjs theme
    holderThumb: function() {
        Holder.addTheme('thumb', {
            bg: '#55595c',
            fg: '#eceeef',
            text: 'Thumbnail'
        });
    },
    // Account form handler
    accountFormHandler : function () {
        $('.form-handler').on('click',function( ) {
            // Index.initLogin();
            $('.login-handler, .register-handler').find('.message').empty();
            // console.log( $('.login-handler, .register-handler').find('message').html() );
            // e.preventDefault();
        });
        //$('.login-account');
    },
    // User Form Login Handler
    loginHandler: function () {
        $('#form-login').submit(function(e) {
            e.preventDefault();
            var inputs = $(this).find('input[class=form-control], button[type=submit]');
            var email = $(this).find('[name=email]').val();
            var password = $(this).find('[name=password]').val();
            var box = $(this).find('.message');
            var $this = $(this);
            var message = 'Not match credentials';
            // Set input on disable first
            inputs.prop('disabled',true).css({'opacity':0.75});
            // Send a POST request
            axios.post(route('auth.login'), {
                email: email,
                password: password
            },
            // Set axios config 
            {
                // `onUploadProgress` allows handling of progress events for uploads
                onUploadProgress: function (progressEvent) {
                    // Do whatever you want with the native progress event
                    // $(this).find('.message').html('Checking..');
                }

            }).then(function (response) {
                var message = (typeof response.data.messages !== 'undefined') ? response.data.messages : '';
                // Close current fancybox instance
                if (response.data.authenticated == 1) {
                    parent.jQuery.fancybox.getInstance().close();
                    window.location.href = route('activity');
                } else if(message) {
                    var display = '<div class="alert alert-warning" role="alert"><span>'+message+'</span></div>';
                    $this.find('.message').html(display);
                    //setInterval(window.location.href = base_URL,3000);
                } else {
                    $this.find('.message').empty();
                }
            })
            .catch(function (error) {
              console.log(error);
            });            
            inputs.prop('disabled',false).css({'opacity':1});
        });
        $('.register-trig').on('click', function(){
           $('.login-handler').hide();
           $('.register-handler').show();
           $('form.message').html();
        });
        $('.login-trig').on('click', function() {
            $('.login-handler').show();
            $('.register-handler').hide();             
            $('form.message').html();
        })
    },
    // User Form Register Handler
    registerHandler: function() {
        $('#form-register').submit(function(e) {
            e.preventDefault();
            var $this = $(this);
            var inputs = $this.find('input[class=form-control], button[type=submit]');
            // ---- Inputs Start
            var first_name = $this.find('[name=first_name]').val();
            var last_name = $this.find('[name=last_name]').val();            
            var email = $this.find('[name=email]').val();
            var password = $this.find('[name=password]').val();
            var password_confirmation = $this.find('[name=password_confirmation]').val();            
            // ---- Inputs End            
            var box = $(this).find('.message');
            var $this = $(this);
            var message = 'Not match credentials';
            // Set input on disable first
            inputs.prop('disabled',true).css({'opacity':0.75});
            // Send a POST request
            axios.post(route('auth.register'), {
                first_name: first_name,
                last_name: last_name,
                email: email,
                password: password,
                password_confirmation: password_confirmation
            },
            // Set axios config 
            {
                // `onUploadProgress` allows handling of progress events for uploads
                onUploadProgress: function (progressEvent) {
                    // Do whatever you want with the native progress event
                    //$this.find('.message').html('Checking..');
                }

            }).then(function (response) {
                // Close current fancybox instance
                var message = ( typeof response.data.messages !== 'undefined' ) ? response.data.messages : '';
                if (response.data.authenticated == 1) {
                    // Close fancybox
                    parent.jQuery.fancybox.getInstance().close();
                    // Redirect
                    window.location.href = route('activity');
                } else if (typeof response.data.errors !== 'undefined') {                    
                    // Set errors variable
                    var errors = response.data.errors ? response.data.errors : '';
                    var display = '<div class="alert alert-warning" role="alert"><span>'+errors+'</span></div>';
                    $this.find('.message').html(display);
                } else if (message) {
                    var display = '<div class="alert alert-warning" role="alert"><span>'+message+'</span></div>';                    
                    $this.find('.message').html(display);
                    //setInterval(window.location.href = base_URL,3000);
                } else {
                    $this.find('.message').html();
                }
            })
            .catch(function (error) {
              console.log(error);
            });            
            inputs.prop('disabled',false).css({'opacity':1});
        });
    },
    // Activity Modals
    activityModal: function () {

    },
    // KOL Modal
    kolModals : function () {
        $("[data-fancybox]").fancybox({
            beforeShow: function( instance, slide ) {
                // $('#hidden-content2 .embed-responsive').empty();
                $('#hidden-content2 .embed-responsive-item').attr('src','');
            },
            afterClose :  function( instance, slide ) {                
                // $('#hidden-content2 .embed-responsive').empty();
                $('#hidden-content2 .embed-responsive-item').attr('src','');
            },
        });

        $('#hidden-content2.fancybox-content > button .fancybox-close-small').click(function (e) {                 
            //$('#hidden-content2 .embed-responsive').empty();
            $('#hidden-content2 .embed-responsive-item').attr('src','');            
            e.preventDefault();
            return false;
        });

        $('a[data-src="#hidden-content2"]').on('click',function (e) {
            var $this = $(this).parents('.card');
            var $name = $this.find('.card-title').html();         
            var $desc = $this.find('.card-text .text-ambass').html();
            var $video_url = $this.find('.card-text').attr('data-url');
            var $video_name = $this.find('.card-text').attr('data-video');
            var $img = $this.find('.img-fluid').attr('src');
            var $popup = $('#hidden-content2');
            $popup.find('.video-head-alt .rounded-circle').attr('src',$img);
            $popup.find('.video-head-alt .text-ambass.text-uppercase').html($name);
            $popup.find('.video-head-img').attr('src',$img);
            $popup.find('.video-head-desc').html($desc);
            $popup.find('.video-head-content').html($video_name);
            $popup.find('.embed-responsive .embed-responsive-item').attr('src','');
            if ($video_url) {
                $popup.find('.embed-responsive-item').attr('src','').attr('src', $video_url + '?autoplay=1');
            }
            e.preventDefault();
            return false;
        });
    },
    // Videos Modals
    videosModal: function () {
        $('.open-modal, .btn-play-video').on('click',function (e) {
            var nav = $(this).attr('href');
            $('#myTab').find(nav+'-tab').click();
            //e.preventDefault();
        });
    },
    // Vote Modals
    voteModal: function () {
        // Set event handler
        $('.open-vote').on('click',function(e) {
            // Set part id and video
            var id_part = $(this).data('id');
            var video = $(this).data('video');
            if (id_part === 'part-0') {
                // Close current fancybox instance
                // parent.jQuery.fancybox.getInstance().close();
                // Click login automate with 
                $(".form-handler").click();
                return false;
            } 

            /*
            var id_part = $(this).data('id');
            var video = $(this).data('video');
            if (id_part === 'part-0') {
                // Close current fancybox instance
                parent.jQuery.fancybox.getInstance().close();
                // Click login automate with 
                $(".form-handler").click();
            } else {
                axios.post(route('video.vote'), {
                    part_id: id_part,
                    video: video
                  })
                  .then(function (response) {
                    // Close current fancybox instance
                    var message = ( typeof response.data.votes !== 'undefined' ) ? response.data.votes : '';
                    //alert(message);
                    if (message) {                       
                        
                        //setInterval(window.location.href = route('video.vote.done'), 3000);
                    }
                  })
                  .catch(function (error) {
                    console.log(error);
                });
            }  
            */
        });
    }
};

$(document).ready(function() {
    // Initiliaze script on ready
    LOTCHOC.init();
    /*
    var addthis_share = {
        url: route('activity'),
        title: "#PremiumMomentstogether",
        description: "Voted",
        media: baseUrl + '/images/logo.png'
    }
    //$.fancybox.open('<div class="message"><h2>Hello!</h2><p>You are awesome!</p></div>');
    var thanks = 'Thank You for Voting, Mom! Terima kasih telah turut merayakan pencapaian baru si Kecil. Pemenang yang beruntung akan mendapat Grand Prize & hadiah menarik lainnya!';
    var shares = '<div class="row col"><a data-original-title="Facebook" rel="tooltip" href="javascript:;" data-placement="left" href="#facebook" title="Login with Facebook" class="btn btn-primary btn-md btn-rounded mt-3 mr-2 addthis_button_facebook" data-url="THE URL" data-url="'+addthis_share.url+'" data-title="'+addthis_share.title+'" data-description="'+addthis_share.description+'" data-media="'+addthis_share.media+'"><span class="fab fa-facebook"></span>&nbsp; Facebook</a>'
    +'<a data-original-title="Facebook" rel="tooltip" href="javascript:;" data-placement="left" href="#twitter" title="Login with Twitter" data-url="'+addthis_share.url+'" data-title="'+addthis_share.title+'" data-description="'+addthis_share.description+'" data-media="'+addthis_share.media+'" class="btn btn-info btn-md btn-rounded mt-3 addthis_button_twitter"><span class="fab fa-twitter"></span>&nbsp; Twitter</a></div>';
    var div = '<div class="bg-brown thanks-pop"><h2 class="main-head-alt">Thank you for your Vote!</h2><div class="row"><div class="col-sm-6"><img src="images/img-thank-you.png" alt="Thank You"></div><div class="col-sm-6 text-white"><h4 class="main-head-alt">Share</h4>'+thanks+' '+shares+'</div></div></div>';
    $.fancybox.open({
        'src' : div,
        'type':'html'
    });
    */
});
// data-original-title="Facebook" rel="tooltip" href="javascript:;" class="btn btn-facebook addthis_button_facebook" data-placement="left"

