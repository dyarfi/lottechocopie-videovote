// jQuery
//import jQuery from 'jquery';
// React Modules
import React from 'react';
import ReactDOM from 'react-dom';

// Component and pages
/**
 * index.js
 * Client-Side JS Bootsrap
 * Loads pages and components
 */
import Login from './components/login.js';
import Register from './components/register.js';
import Social from './components/social.js';

// jQuery Init
//const $ = jQuery;
//const $body = $('body');

export default {
    // init: ()=>{
    //   //if ($body.hasClass('main-page')){
    //     ReactDOM.render(<div><Login /><Register /><Social /></div>, document.getElementById('react__root'));
    //     console.log('Init');
    //   //}
    // }
    initLogin: () => {
      ReactDOM.render(<div><Login /></div>, document.getElementById('hidden-content'));
    }
  }