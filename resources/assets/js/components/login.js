import React, { Component} from 'react';

class Login extends Component {
  render(){
    return(
	<div>	
		<h4 className="container font-weight-bold py-2">Login</h4>
        <form method="POST" action="//localhost:3000/auth/login" acceptCharset="UTF-8" className="from-horizontal" id="form-login"><input name="_token" type="hidden" value="WQcU4YSLsOPgQsEGysPxNyHAioVjtYzSsQCdtT4S"/>
		<div className="container"><div className="message"></div></div>
			<div className="form-group">
				<div className="col-md-12">
                    <label className="col-md-4 control-label sr-only">E-Mail Address</label>
                    <div className="input-group input-group-lg mb-3">
                        <div className="input-group-prepend"><span className="input-group-text text-warning"><i className="fa fa-envelope"></i></span></div>
                        <input type="email" className="form-control" placeholder="Email" name="email" value=""/>
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="col-md-12">
                    <label className="col-md-4 control-label sr-only">Password</label>
                    <div className="input-group input-group-lg mb-3">
                        <div className="input-group-prepend"><span className="input-group-text text-warning"><i className="fa fa-lock"></i></span></div>
                        <input type="password" className="form-control" placeholder="Password" name="password"/>
                        <div className="input-group-append"><span className="input-group-text text-warning"><a className="text-warning small" href="//localhost:3000/auth/password/email">Lupa password?</a></span></div>
                    </div>
                </div>
            </div>
            <div className="form-group">
                <div className="col-md-12">
                    <div className="checkbox">
                        <label>
                            <input type="checkbox" name="remember"/> Remember Me
                        </label>
                    </div>
                </div>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-md-4">
                        <button type="submit" className="btn btn-warning">Login</button>
                    </div>
                    <div className="col-md-8 text-white">
                        <div className="float-right">
                            Belum punya akun ?
                            <a className="text-warning" href="//localhost:3000/auth/register">
                            Sign Up ?
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <h4 className="text-center text-warning">Atau</h4>
                <h5 className="text-white py-3">Login dengan Sosial Media</h5>
                <div className="row">
                    <div className="col-md-12 col-xs-12 col-md-offset-4">
                        <a href="//localhost:3000/auth/social/twitter" title="Login with Twitter" className="btn btn-info btn-md btn-rounded"><span className="fab fa-twitter"></span>&nbsp; Twitter</a>
                        <a href="//localhost:3000/auth/social/facebook" title="Login with Facebook" className="btn btn-primary btn-md btn-rounded"><span className="fab fa-facebook"></span>&nbsp; Facebook</a>
                    </div>
                </div>
            </div>
        </form>
	</div>
    );
  }
}

export default Login;
