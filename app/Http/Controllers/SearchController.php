<?php namespace App\Http\Controllers;

use App\Modules\News\Model\News;
use DB, Input;

class SearchController extends BasePublic {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

		// Parent constructor
		parent::__construct();

		//$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{		
		return redirect()->intended('search/'.Input::get('query'));
	}

	/**
	 * Show the search query page.
	 *
	 * @return View
	 */
	public function search($query_text='')
	{
		$collections = News::where('name', 'like', '%'.$query_text.'%')		
		->where('excerpt', 'like', '%'.$query_text.'%')
		->where('description', 'like', '%'.$query_text.'%')
		->where('status', 1)
		->take(95)->paginate(2);
		
		// Set data
		$data = ['query'=>$query_text,'collections'=>$collections];

		return $this->view('search')->data($data)->title('Search for :' . $query_text);
	}


}
