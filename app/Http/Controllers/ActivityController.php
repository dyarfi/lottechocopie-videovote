<?php namespace App\Http\Controllers;

// Load Laravel classes
use Route, Request, Input, Validator, Redirect, Session;
// Load main models
use App\Modules\Page\Model\Menu, 
App\Modules\Page\Model\Page,
App\Modules\Banner\Model\Banner,
App\Modules\Campaign\Model\Video,
App\Modules\Campaign\Model\Event,
App\Modules\Campaign\Model\Ambassador,
App\Modules\News\Model\News,
App\Modules\Participant\Model\Vote,
App\Modules\Participant\Model\Participant;

class ActivityController extends BasePublic {

	// Participant / User set default
	public $participant = '';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Parent constructor
		parent::__construct();

	}

	public function home()
	{
		return $this->view('pages.home')->title('Home - Laravel Tasks');
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($done='')
	{
		// dd($done);
		// Get the page path that requested
		$path = pathinfo(Request::path(), PATHINFO_BASENAME);

		// Set data to return
		$data = [
			'menu'=>$this->menu->where('slug', $path)->first(),
			//'videos'=>Video::where('status',1)->orderBy('created_at')->take(100)->get(),
			'videos'=>Video::where('status',1)->with('votes')->orderBy('created_at')->take(100)->limit(3)->get(),
			'banners'=>Banner::where('status',1)->orderBy('created_at')->take(100)->get(),
			'events'=>Event::where('status',1)->orderBy('created_at')->take(100)->get(),
			'ambassadors'=>Ambassador::where('status',1)->with('video')->orderBy('created_at')->take(100)->get(),
			'news_list'=>News::where('status',1)->orderBy('created_at')->limit(3)->get(),
			'welcome'=>Page::find(3)			
		];

		// Set open graph		
		$ogs = [
			'og:title' => 'Lotte Choco Pie - #PremiumMomentstogether',
			'og:description' => 'Lotte Choco Pie mengapresiasi proses tumbuh kembang si Kecil, maka dari itu melalui #PremiumMomentstogether kami mengajak Mom berbagi cerita pertumbuhan si Kecil dengan memilih cerita yang sesuai pengalaman. Grand prize trip ke Jepang & hadiah menarik lain bisa dimenangkan!',
			'og:image' => asset('images/img-banner-activity.jpg')
		];
		
		// Set views
		return $this->view('menus.activity')->data($data)
		->ogs($ogs)
		->title('Page | Activity - Lotte Choco Pie - #PremiumMomentstogether');
		
	}

	public function vote () {

		$request = Request::all();

		$video = Video::slug($request['video']);
		
		$votes = Vote::create([
			'participant_id'=> $this->user->id,
			'attribute'=>$video->id,
			'status'=>1
		]);
		
		// Check data
		if($votes && Request::ajax()) {
			return response()->json([
				'votes' => $votes,
				'video' => $video
			]);
		} else {
			return response()->json([
				'votes' => NULL
			]);
		}

	}

	public function voteGet ($slug="") {
		// Find video first
		$video = Video::where('slug',$slug)->first();
		// Check the video and participant
		if($video && !$this->user['vote']) {
			// Flash Messages
			Request::session()->flash('flash_thankyou', 'Voted!');
			// Insert vote
			$votes = Vote::create([
				'participant_id'=> $this->user['id'],
				'attribute'=>$video->id,
				'status'=>1
			]);
		} else {
			// Flash Messages
			Request::session()->flash('flash_thankyou', 'Sorry no can do!');
		}
		// Redirect to redirect path
		return redirect()->intended('activity');
	}

}
