<?php namespace App\Http\Controllers;

// Load Laravel classes
use Request;

// Load main models
use App\Modules\Page\Model\Menu, 
	App\Modules\Page\Model\Page,
	App\Modules\Portfolio\Model\Portfolio,
	App\Modules\Banner\Model\Banner;

class ProductController extends BasePublic {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Parent constructor
		parent::__construct();

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		// Get the page path that requested
		$path = pathinfo(Request::path(), PATHINFO_BASENAME);

		// Set data to return
		$data = [
			'menu'=>$this->menu->where('slug', $path)->first(),
			'banners'=>Banner::where('status',1)->orderBy('created_at')->take(100)->get(),
		];

		// Set open graph		
		$ogs = [
			'og:title' => 'Lotte Choco Pie - #PremiumMomentstogether',
			'og:description' => 'Lotte Choco Pie mengapresiasi proses tumbuh kembang si Kecil, maka dari itu melalui #PremiumMomentstogether kami mengajak Mom berbagi cerita pertumbuhan si Kecil dengan memilih cerita yang sesuai pengalaman. Grand prize trip ke Jepang & hadiah menarik lain bisa dimenangkan!',
			'og:image' => asset('images/img-banner-activity.jpg')
		];

		// Return views
		return $this->view('menus.product')
		->data($data)
		->ogs($ogs)
		->title('Page | Product');		
	}

}
