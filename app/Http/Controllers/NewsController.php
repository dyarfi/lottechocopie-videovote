<?php namespace App\Http\Controllers;

// Load Laravel classes
use Request, File;

// Load main models
use App\Modules\Page\Model\Menu, App\Modules\Page\Model\Page;
use App\Modules\News\Model\News, App\Modules\News\Model\NewsCategory;

class NewsController extends BasePublic {


	//public $restful = true;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Parent constructor
		parent::__construct();

		//$this->middleware('auth');

		//$this->middleware('language');

		//dd(Auth::inRole('admin'));
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		// Get the page path that requested
		$path = pathinfo(Request::path(), PATHINFO_BASENAME);

		// Get listings on this pages
		$news = News::where('status', 1)
            ->orderBy('created_at', 'DESC')
            ->first();
		
			// Set news lists
		$news_list = News::where('status', 1)
            ->where('uuid','!=', $news->uuid)
            ->orderBy('created_at', 'DESC')
            ->take(200)
            ->paginate(6);

		// Set data to return
		$data = [
            'menu' => $this->menu->where('slug', $path)->first(),
            'news' => $news,
            'news_list' => $news_list
		];
		
		// Set open graph		
		$ogs = [
			'og:title' => 'Lotte Choco Pie - #PremiumMomentstogether',
			'og:description' => 'Lotte Choco Pie mengapresiasi proses tumbuh kembang si Kecil, maka dari itu melalui #PremiumMomentstogether kami mengajak Mom berbagi cerita pertumbuhan si Kecil dengan memilih cerita yang sesuai pengalaman. Grand prize trip ke Jepang & hadiah menarik lain bisa dimenangkan!',
			'og:image' => asset('images/img-banner-activity.jpg')
		];
				
		return $this->view('menus.news')
			->data($data)
			->ogs($ogs)
            ->title('Page | News &amp; Event');
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($slug)
	{

		// Get data from database
		$news = News::where('slug',$slug)->first();
		
		// Set news lists
        $news_lists = News::active()->where('slug','!=',$slug)->get();

		// Set data to return
	   	$data = [
			'news'=>$news,
			'news_lists' => $news_lists
		];
		
		// Set open graph		
		$ogs = [
			'og:title' => @$news->name,
			'og:description' => str_limit(strip_tags($news->excerpt),300,''),
			'og:image' => File::exists(public_path('uploads/'.$news->image)) ? asset('uploads/'.$news->image) : asset('images/img-banner-activity.jpg')
		];

	   	// Return data and view
	   	return $this->view('news.show')->data($data)->ogs($ogs)->title('Page | News Detail');

	}

}
