<?php namespace App\Http\Controllers;

// use Mail;
// Load modules
use App\Modules\Page\Model\Page;
use App\Modules\Banner\Model\Banner;
use App\Modules\Campaign\Model\Video;
use App\Modules\Campaign\Model\Event;
use App\Modules\Campaign\Model\Ambassador;
use App\Modules\News\Model\News;

class HomeController extends BasePublic  {


	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "homepage" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		// User this for auth and socialite use
		// $this->middleware('auth');

		// Env
		//print_r(env('APP_ENV'));

	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{

		$data = [
			//'videos'=>Video::where('status',1)->orderBy('created_at')->take(100)->get(),
			'banners' => Banner::where('status',1)->orderBy('created_at')->take(100)->get(),
			//'events'=>Event::where('status',1)->orderBy('created_at')->take(100)->get(),
			//'ambassadors'=>Ambassador::where('status',1)->orderBy('created_at')->take(100)->get(),
			'news_list' => News::where('status',1)->orderBy('created_at', 'DESC')->limit(2)->get(),
			'welcome' => Page::find(3)
		];

		// Set open graph		
		$ogs = [
			'og:title' => 'Lotte Choco Pie - #PremiumMomentstogether',
			'og:description' => 'Lotte Choco Pie mengapresiasi proses tumbuh kembang si Kecil, maka dari itu melalui #PremiumMomentstogether kami mengajak Mom berbagi cerita pertumbuhan si Kecil dengan memilih cerita yang sesuai pengalaman. Grand prize trip ke Jepang & hadiah menarik lain bisa dimenangkan!',
			'og:image' => asset('images/img-banner-activity.jpg')
		];

		return $this->view('home')
		->data($data)
		->ogs($ogs)
		->title('Home'); //- See more at: http://laravelsnippets.com/snippets/base-controller-extended#sthash.qTHFuvbZ.dpuf

	}

}
