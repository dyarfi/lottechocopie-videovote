<?php namespace App\Modules\Campaign\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Validator, View, Excel, Image, Storage, File;
use Illuminate\Validation\Rule;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Campaign\Model\Video;
// Load Datatable
use Datatables;
// User Activity Logs
use Activity;

class Videos extends BaseAdmin {

	/**
	 * Set videos data.
	 *
	 */
	protected $videos;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// dd(storage_path('app/public/uploads'));

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin',['except'=>'profile']);

		// Load videos and get repository data from Auth
		$this->videos = new Video;

		// Crop to fit image size
		$this->imgFit 		= ['512x490'];

	}

	/**
	 * Display a listing of videos.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index()
	{

		// Set return data
	   	$videos = Input::get('path') === 'trashed' ? $this->videos->with('votes')->onlyTrashed()->get() : $this->videos->with('votes')->orderBy('created_at','desc')->get();

		// Get deleted count
		$deleted = $this->videos->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows'=>$videos,'deleted'=>$deleted,'junked'=>Input::get('path')];

		// Load needed scripts
		$scripts = [
				'dataTables' => asset('themes/ace-admin/js/jquery.dataTables.min.js'),
				'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
				'library' => asset("themes/ace-admin/js/library.js")
					];

		// Set inline script or style
		$inlines = [
		// Script execution on a specific controller page
		'script' => "
		// --- datatable handler [".route('admin.videos.index')."]--- //
			var datatable  = $('#datatable-table');
			var controller = datatable.attr('rel');

			$('#datatable-table').DataTable({
				processing: true,
				serverSide: true,
				bAutoWidth: false,
				ajax: '".route('admin.videos.datatable')."' + ($.getURLParameter('path') ? '?path=' + $.getURLParameter('path') : ''),
				columns: [
					{data: 'id', name:'id', orderable: false, searchable: false},
					{data: 'name', name: 'name'},
					{data: 'description', name: 'description'},
					{data: 'embed', name: 'embed'},
					{data: 'votes', name: 'votes'},
					{data: 'status', name: 'status'},
					{data: 'created_at', name: 'created_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					processing: ''
				},
				fnDrawCallback : function (oSettings) {
					$('#datatable-table > thead > tr > th:first-child')
					.removeClass('sorting_asc')
					.find('input[type=checkbox]')
					.prop('checked',false);
					$('#datatable-table > tbody > tr > td:first-child').addClass('center');
					$('[data-rel=tooltip]').tooltip();
				}
			});
		",
		];

		return $this->view('Campaign::video_datatable_index')
		->data($data)
		->scripts($scripts)
		->inlines($inlines)
		->title('Videos List');
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function datatable(Request $request)
	{
		$rows = Input::get('path') === 'trashed' ? $this->videos->onlyTrashed()->get() : $this->videos->orderBy('created_at', 'asc')->get();

		return Datatables::of($rows)
			// Set action buttons
			->editColumn('action', function ($row) {
				if (Input::get('path') !== 'trashed') {
					return '
						<a data-rel="tooltip" data-original-title="View" title="" href="'.route('admin.videos.show', $row->id).'" class="btn btn-xs btn-success tooltip-default">
							<i class="ace-icon fa fa-check bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Edit"  href="'.route('admin.videos.edit', $row->id).'" class="btn btn-xs btn-info tooltip-default">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Trashed"  href="'.route('admin.videos.trash', $row->id).'" class="btn btn-xs btn-danger tooltip-default">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</a>';
				} else {
					return '
						<a data-rel="tooltip" data-original-title="Restore!" href="'.route('admin.videos.restored', $row->id).'" class="btn btn-xs btn-primary tooltip-default">
							<i class="ace-icon fa fa-save bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Permanent Delete!" href="'.route('admin.videos.delete', $row->id).'" class="btn btn-xs btn-danger">
							<i class="ace-icon fa fa-trash bigger-120"></i>
						</a>';
				}
			})
			// Edit column name
			->editColumn('name', function ($row) {
				return $row->name;
			})
			// Edit column id
			->editColumn('id', function ($row) {
				return 	'
				<label class="pos-rel">
					<input type="checkbox" class="ace" name="check[]" id="check_'.$row->id.'" value="'.$row->id.'" />
					<span class="lbl"></span>
				</label>';
			})
			// Set description limit
			->editColumn('description', function ($row) {
				return str_limit(strip_tags($row->description), 60);
			})
			// Edit column image
			->editColumn('votes', function ($row) {
				// $html = '<a data-rel="colorbox" class="cboxElement" href="'.asset('uploads/512x490px_'.$row->image).'"><img src="'.asset('uploads/'.$row->image).'" height="50px"/></a>';
				// return $html;
				return $row->votes->count();
			})
			// Set status icon and text
			->editColumn('status', function ($row) {
				return '
				<span class="label label-'.($row->status == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->status == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.config('setting.status')[$row->status].'
				</span>';
            })
			->rawColumns(['id','action','embed','image','status'])
			->make(true);
	}
	
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
        $video = $this->videos->findOrFail($id);

        // Read ACL settings config for any permission access
        $acl = config('setting.modules');

		// Set data to return
	   	$data = ['row'=>$video,'acl'=>$acl];

	   	// Return data and view
	   	return $this->view('Campaign::video_show')->data($data)->title('View Video');

	}

	/**
	 * Show the form for creating new video.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new video.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating video.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating video.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified video.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($video = $this->videos->find($id))
		{

			// Add deleted_at and not completely delete
			$video->delete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.videos.index'))->with('success', 'Video Trashed!');
		}

		return Redirect::to(route('admin.videos.index'))->with('error', 'Video Not Found!');
	}

	/**
	 * Restored the specified video.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($video = $this->videos->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$video->restore();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.videos.index'))->with('success', 'Video Restored!');
		}

		return Redirect::to(route('admin.videos.index'))->with('error', 'Video Not Found!');
	}
	/**
	 * Remove the specified video.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{

		// Get video from id fetch
		if ($video = $this->videos->onlyTrashed()->find($id))
		{

			// Delete if there is an image attached
			if(File::exists('uploads/'.$video->image)) {
				// Delete the single file
				File::delete('uploads/'.$video->image);

			}

			// Permanently delete
			$video->forceDelete();

			// Log it first
			Activity::log(__FUNCTION__);

			return Redirect::to(route('admin.videos.index','path=trashed'))->with('success', 'Video Permanently Deleted!');
		}

		return Redirect::to(route('admin.videos.index','path=trashed'))->with('error', 'Video Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{

		if ($id)
		{
			if ( ! $row = $this->videos->find($id))
			{
				return Redirect::to(route('admin.videos.index'));
			}
		}
		else
		{
			$row = $this->videos;
		}

		return $this->view('Campaign::video_form')->data(compact('mode', 'row'))->title('Video '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null)
	{
		$input = array_filter(Input::all());

		// Set blog slug
		$input['slug'] = isset($input['name']) ? str_slug($input['name'],'-') : '';
		
		$rules = [
			'name' => 'required'.($id) ? '' : '|unique:videos,name',
			'description'  => 'required',
			'image' 	   => ($mode == 'create' ? 'required|' : '').'mimes:jpg,jpeg,png|max:999',
			'url'      => 'required',
			'status'	 => 'boolean'
		];

		if ($id)
		{
			
			$video = Video::find($id);

			$messages = $this->validateVideo($input, $rules);

			// If user upload a file
			if (isset($input['image']) && Input::hasFile('image')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['image'], 'uploads', 'videos_');

			}

			if ($messages->isEmpty())
			{

				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				// Slip image file
				$result = isset($filename) ? array_set($input, 'image', $filename) : $result;

				// Get video model to update other profile data
				$video->update($input);

				return Redirect::back()->withInput()->with('success', 'Video Updated!');

			}
		}
		else
		{

			$messages = $this->validateVideo($input, $rules);

			// If user upload a file
			if (isset($input['image']) && Input::hasFile('image')) {

				// Set filename
				$filename = $this->imageUploadToDb($input['image'], 'uploads', 'videos_');

			}

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Slip user id
				$result = array_set($result, 'user_id', $this->user->id);

				// Slip image file
				$result = isset($input['image']) ? array_set($result, 'image', @$filename) : array_set($result, 'image', '');

				// Create video into the database
				$video = Video::create($input);

			}
		}

		// Log it first
		Activity::log(__FUNCTION__);

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.videos.show',$video->id))->with('success', 'Video Updated!');
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {

		// Log it first
		Activity::log(__FUNCTION__);

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->videos->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.videos.index'))->with('success', 'Video Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.videos.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Validates a video.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateVideo($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}

	/**
	 * Process a file upload save the filename to DB.
	 *
	 * @param  array  $file
	 * @param  string $path
	 * @param  string $type
	 * @return $filename
	 */
	protected function imageUploadToDb($file='', $path='', $type='')
	{
		// Set filename upload
		$filename = '';
		// Check if input and upload already assigned
		if (!empty($file) && !$file->getError()) {
			// Getting image extension
			$extension = $file->getClientOriginalExtension();
			// Renaming image
			$filename = $type . rand(11111,99999) . '.' . $extension;
			// Set intervention image for image manipulation
			Storage::disk('local_uploads')->put($filename,
				file_get_contents($file)
			);
			// If image has a resize crop data in constructor
			if (!empty($this->imgFit)) {
			  $image = Image::make($path .'/'. $filename);
			  // backup status
			  $image->backup();
				foreach ($this->imgFit as $imgFit) {
					$size = explode('x',$imgFit);
					$image->fit($size[0],$size[1])->save($path .'/'. $imgFit.'px_'. $filename,100)->reset();
				}
			}
		}
		return $filename;
  	}

	/**
	 * Process a file to download.
	 *
	 * @return $file export
	 */
	public function export() {

		// Log it first
		Activity::log(__FUNCTION__);

		// Get type file to export
		$type = Input::get('rel');
		// Get data to export
		$videos = $this->videos->select('id','name','description','status','updated_at','created_at')->get();
		// Export file to type
		Excel::create('videos', function($excel) use($videos) {
			// Set the spreadsheet title, creator, and description
	        $excel->setTitle('Export List');
	        $excel->setCreator('Laravel')->setCompany('laravel.com');
	        $excel->setDescription('export file');

		    $excel->sheet('Sheet 1', function($sheet) use($videos) {
				$sheet->fromArray($videos);
		    });
		})->export($type);

	}

}
