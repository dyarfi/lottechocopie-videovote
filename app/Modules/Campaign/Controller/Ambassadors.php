<?php namespace App\Modules\Campaign\Controller;

// Load Laravel classes
use Route, Request, Session, Redirect, Input, Image, Validator, View, File;
// Load main base controller
use App\Modules\BaseAdmin;
// Load main models
use App\Modules\Campaign\Model\Ambassador;
// Load main models
use App\Modules\Campaign\Model\Video;
// Load Datatable
use Datatables;
// User Activity Logs
use Activity;

class Ambassadors extends BaseAdmin {

	/**
	 * Set ambassadors data.
	 *
	 */
	protected $ambassadors;

	/**
	 * Set videos data.
	 *
	 */
	protected $videos;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//dd(storage_path());

		// Parent constructor
		parent::__construct();

		// Load Http/Middleware/Admin controller
		$this->middleware('auth.admin');

		// Load ambassadors and get repository data from database
		$this->ambassadors = new Ambassador;
		$this->videos = new Video;

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index() {

		// Set return data
	   	$ambassadors = Input::get('path') === 'trashed' ? $this->ambassadors->with('video')->onlyTrashed()->get() : $this->ambassadors->with('video')->orderBy('created_at','desc')->get();
		
		// Get deleted count
		$deleted = $this->ambassadors->onlyTrashed()->get()->count();

	   	// Set data to return
	   	$data = ['rows' => $ambassadors,'deleted' => $deleted,'junked' => Input::get('path')];
		/*
		// Load needed scripts
	   	$scripts = [
	   				'dataTables'=> 'themes/ace-admin/js/jquery.dataTables.min.js',
	   				'dataTableBootstrap'=> 'themes/ace-admin/js/jquery.dataTables.bootstrap.min.js',
	   				'dataTableTools'=> 'themes/ace-admin/js/dataTables.tableTools.min.js',
	   				'dataTablesColVis'=> 'themes/ace-admin/js/dataTables.colVis.min.js',
				   	// Load needed javascripts
					'library' => asset('themes/ace-admin/js/library.js')
	   				];

		// Return data and view
		return $this->view('Campaign::ambassador_index')->data($data)->scripts($scripts)->title('Ambassador List');
		*/
		// Load needed scripts
		$scripts = [
				'dataTables' => asset('themes/ace-admin/js/jquery.dataTables.min.js'),
				'dataTableBootstrap'=> asset('themes/ace-admin/js/jquery.dataTables.bootstrap.min.js'),
				'library' => asset("themes/ace-admin/js/library.js")
					];

		// Set inline script or style
		$inlines = [
		// Script execution on a specific controller page
		'script' => "
		// --- datatable handler [".route('admin.ambassadors.index')."]--- //
			var datatable  = $('#datatable-table');
			var controller = datatable.attr('rel');

			$('#datatable-table').DataTable({
				processing: true,
				serverSide: true,
				bAutoWidth: false,
				ajax: '".route('admin.ambassadors.datatable')."' + ($.getURLParameter('path') ? '?path=' + $.getURLParameter('path') : ''),
				columns: [
					{data: 'id', name:'id', orderable: false, searchable: false},
					{data: 'name', name: 'name'},
					{data: 'video', name: 'video'},
					{data: 'description', name: 'description'},
					{data: 'status', name: 'status'},
					{data: 'created_at', name: 'created_at'},
					{data: 'action', name: 'action', orderable: false, searchable: false}
				],
				language: {
					processing: ''
				},
				fnDrawCallback : function (oSettings) {
					$('#datatable-table > thead > tr > th:first-child')
					.removeClass('sorting_asc')
					.find('input[type=checkbox]')
					.prop('checked',false);
					$('#datatable-table > tbody > tr > td:first-child').addClass('center');
					$('[data-rel=tooltip]').tooltip();
				}
			});
		",
		];

		return $this->view('Campaign::ambassador_datatable_index')
		->data($data)
		->scripts($scripts)
		->inlines($inlines)
		->title('Ambassador List');
	}

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function datatable(Request $request)
	{
		$rows = Input::get('path') === 'trashed' ? $this->ambassadors->with('video')->onlyTrashed()->get() : $this->ambassadors->with('video')->orderBy('created_at', 'asc')->get();
		
		return Datatables::of($rows)
			// Set action buttons
			->editColumn('action', function ($row) {
				if (Input::get('path') !== 'trashed') {
					return '
						<a data-rel="tooltip" data-original-title="View" title="" href="'.route('admin.ambassadors.show', $row->id).'" class="btn btn-xs btn-success tooltip-default">
							<i class="ace-icon fa fa-check bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Edit"  href="'.route('admin.ambassadors.edit', $row->id).'" class="btn btn-xs btn-info tooltip-default">
							<i class="ace-icon fa fa-pencil bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Trashed"  href="'.route('admin.ambassadors.trash', $row->id).'" class="btn btn-xs btn-danger tooltip-default">
							<i class="ace-icon fa fa-trash-o bigger-120"></i>
						</a>';
				} else {
					return '
						<a data-rel="tooltip" data-original-title="Restore!" href="'.route('admin.ambassadors.restored', $row->id).'" class="btn btn-xs btn-primary tooltip-default">
							<i class="ace-icon fa fa-save bigger-120"></i>
						</a>
						<a data-rel="tooltip" data-original-title="Permanent Delete!" href="'.route('admin.ambassadors.delete', $row->id).'" class="btn btn-xs btn-danger">
							<i class="ace-icon fa fa-trash bigger-120"></i>
						</a>';
				}
			})
			// Edit column name
			->editColumn('name', function ($row) {
				return $row->name;
			})
			// Edit column video
			->editColumn('video', function ($row) {
				$video = ($row->video) ? $row->video->url : '';
				return $video ? '<div style="position:relative;height:0;padding-bottom:80.63%"><iframe src="'.$video.'?rel=0&controls=0&showinfo=0"style="position:absolute;width:100%;height:100%;left:0" width="886" height="360" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div><div class="text-center">'.@$row->video->name.'</div>' : '-';
			})
			// Edit column id
			->editColumn('id', function ($row) {
				return 	'
				<label class="pos-rel">
					<input type="checkbox" class="ace" name="check[]" id="check_'.$row->id.'" value="'.$row->id.'" />
					<span class="lbl"></span>
				</label>';
			})
			// Set description limit
			->editColumn('description', function ($row) {
				return str_limit(strip_tags($row->description), 60);
			})
			// Set status icon and text
			->editColumn('status', function ($row) {
				return '
				<span class="label label-'.($row->status == 1 ? 'success' : 'warning').' arrowed-in arrowed-in-right">
					<span class="fa fa-'.($row->status == 1 ? 'flag' : 'exclamation-circle').' fa-sm"></span>
					'.config('setting.status')[$row->status].'
				</span>';
            })
			->rawColumns(['id','video','action','status'])
			->make(true);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		// Get data from database
		$ambassador = $this->ambassadors->with('video')->find($id);
		
		// Set data to return
	   	$data = ['row'=>$ambassador];

	   	// Return data and view
	   	return $this->view('Campaign::ambassador_show')->data($data)->title('View Ambassador');

	}

	/**
	 * Show the form for creating new ambassador.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create()
	{
		return $this->showForm('create');
	}

	/**
	 * Handle posting of the form for creating new ambassador.
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function store()
	{
		return $this->processForm('create');
	}

	/**
	 * Show the form for updating ambassador.
	 *
	 * @param  int  $id
	 * @return mixed
	 */
	public function edit($id)
	{
		return $this->showForm('update', $id);
	}

	/**
	 * Handle posting of the form for updating ambassador.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function update($id)
	{
		return $this->processForm('update', $id);
	}

	/**
	 * Remove the specified ambassador.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function trash($id)
	{
		if ($ambassador = $this->ambassadors->find($id))
		{
			// Add deleted_at and not completely delete
			$ambassador->delete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.ambassadors.index'))->with('success', 'Ambassador Trashed!');
		}

		return Redirect::to(route('admin.ambassadors.index'))->with('error', 'Ambassador Not Found!');
	}

	/**
	 * Restored the specified ambassador.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function restored($id)
	{
		if ($ambassador = $this->ambassadors->onlyTrashed()->find($id))
		{

			// Restored back from deleted_at database
			$ambassador->restore();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.ambassadors.index'))->with('success', 'Ambassador Restored!');
		}

		return Redirect::to(route('admin.ambassadors.index'))->with('error', 'Ambassador Not Found!');;
	}

	/**
	 * Delete the specified ambassador.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function delete($id)
	{
		if ($ambassador = $this->ambassadors->onlyTrashed()->find($id))
		{

			// Delete if there is an image attached
			if(File::exists('uploads/'.$ambassador->image)) {
				// Delete the single file
				File::delete('uploads/'.$ambassador->image);

			}

			// Completely delete from database
			$ambassador->forceDelete();

			// Log it first
			Activity::log(__FUNCTION__);

			// Redirect with messages
			return Redirect::to(route('admin.ambassadors.index','path=trashed'))->with('success', 'Ambassador Permanently Deleted!');
		}

		return Redirect::to(route('admin.ambassadors.index','path=trashed'))->with('error', 'Ambassador Not Found!');
	}

	/**
	 * Shows the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return mixed
	 */
	protected function showForm($mode, $id = null)
	{
		
		// Check id
		if ($id)
		{
			if ( ! $row = $this->ambassadors->find($id))
			{
				return Redirect::to(route('admin.ambassadors.index'))->withErrors('Not found data!');;
			}
		}
		else
		{
			$row = $this->ambassadors;
		}

		// Get videos data
		$videos = $this->videos->active()->pluck('name','id')->all();
		
		// Load needed javascripts
		//$scripts = ['bootstrap-datepicker'=> 'themes/ace-admin/js/bootstrap-datepicker.min.js'];
		$scripts= [];
		
		// Load needed stylesheets
		//$styles = ['stylesheet'=> 'themes/ace-admin/c	ss/datepicker.min.css'];
		$styles = [];

		return $this->view('Campaign::ambassador_form')->data(compact('mode', 'row', 'videos'))->scripts($scripts)->styles($styles)->title('Ambassador '.$mode);
	}

	/**
	 * Processes the form.
	 *
	 * @param  string  $mode
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function processForm($mode, $id = null) {

		$input = array_filter(Input::all());

		// Set blog slug
		$input['slug'] = isset($input['name']) ? str_slug($input['name'],'-') : '';
		
		$rules = [
			'name' 	   	   => 'required',
			'attribute'	   => 'required',
			'slug' 		   => ($id) ? 'required' : '',
			'description'  => 'required',
			'status'	   => 'boolean'
		];

		// Set slug
		$input['slug'] = str_slug($input['name'],'-');

		// Set user id
		$input['user_id'] = $this->user->id;

		if ($id)
		{
			$ambassador = $this->ambassadors->find($id);

			$messages = $this->validateAmbassador($input, $rules);

			// checking file is valid.
		    //if ($request->file('image') && $request->file('image')->isValid()) {
			if (!empty($input['image']) && !$input['image']->getError()) {
		      	$destinationPath = public_path().'/uploads'; // upload path
	      		$extension = $input['image']->getClientOriginalExtension(); // getting image extension
		      	$fileName = rand(11111,99999).'.'.$extension; // renaming image
		      	$input['image']->move($destinationPath, $fileName); // uploading file to given path

				/*
				$imagePath = $request->file('image')->store('public');
			    $image = Image::make(Storage::get($imagePath))->resize(320,240)->encode();
			    Storage::put($imagePath,$image);

			    $imagePath = explode('/',$imagePath);

			    $imagePath = $imagePath[1];

			    $myTheory->image = $imagePath;
				*/
		      	//Storage::disk('local')->put('public/'.$fileName, File::get($input['image']));
		      	//Storage::disk('local')->put($fileName,  File::get( $input['image'] ));
		      	// sending back with message
		      	//Session::flash('success', 'Upload successfully');
		      	//return Redirect::to(route('admin.apanel.ambassadors.create'));
		    }
		    else {
			      // sending back with error message.
			      // Session::flash('error', 'uploaded file is not valid');
			      // return Redirect::to('ambassadors/'.$id.'/edit');
		    	  $fileName = old('image') ? old('image') : $ambassador->image;
		    }

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Slip image file
				$result = array_set($result, 'image', $fileName);

				$ambassador->update($result);
			}

		}
		else
		{
			$messages = $this->validateAmbassador($input, $rules);
			// checking file is valid.
		    if (!empty($input['image']) && !$input['image']->getError()) {
		      $destinationPath = public_path().'/uploads'; // upload path
		      $extension = $input['image']->getClientOriginalExtension(); // getting image extension
		      $fileName = rand(11111,99999).'.'.$extension; // renameing image
		      $input['image']->move($destinationPath, $fileName); // uploading file to given path
		      // sending back with message
		      //Session::flash('success', 'Upload successfully');
		      //return Redirect::to(route('admin.apanel.ambassadors.create'));
		    }
		    //else {
		      // sending back with error message.
		      //Session::flash('error', 'uploaded file is not valid');
		      //return Redirect::to(route('admin.ambassadors.create'));
		    //}

			if ($messages->isEmpty())
			{
				// Get all request
				$result = $input;

				// Set user id
				//$result['user_id'] = $this->user->id;

				// Slip image file
				$result = is_array(@$result['image']) ? array_set($result, 'image', '') : array_set($result, 'image', @$fileName);

				//$ambassador = $this->ambassadors->create($input);
				$ambassador = $this->ambassadors->create($result);

			}
		}

		// Log it first
		Activity::log(__FUNCTION__);

		if ($messages->isEmpty())
		{
			return Redirect::to(route('admin.ambassadors.show', $ambassador->id))->with('success', 'Ambassador Updated!');
		}

		return Redirect::back()->withInput()->withErrors($messages);
	}

	/**
	 * Change the data status.
	 *
	 * @param  int     $id
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function change() {

		// Log it first
		Activity::log(__FUNCTION__);

		if (Input::get('check') !='') {

		    $rows	= Input::get('check');

		    foreach ($rows as $row) {
				// Set id for load and change status
				$this->ambassadors->withTrashed()->find($row)->update(['status' => Input::get('select_action')]);
		    }

		    // Set message
		    return Redirect::to(route('admin.ambassadors.index'))->with('success', 'Ambassador Status Changed!');

		} else {

		    // Set message
		    return Redirect::to(route('admin.ambassadors.index'))->with('error','Data not Available!');
		}
	}

	/**
	 * Validates a ambassador.
	 *
	 * @param  array  $data
	 * @param  mixed  $id
	 * @return \Illuminate\Support\MessageBag
	 */
	protected function validateAmbassador($data, $rules)
	{
		$validator = Validator::make($data, $rules);

		$validator->passes();

		return $validator->errors();
	}

	/**
	 * Process a file to download.
	 *
	 * @return $file export
	 */
	public function export() {

		// Log it first
		Activity::log(__FUNCTION__);

		// Get type file to export
		$type = Input::get('rel');
		// Get data to export
		$ambassadors = $this->ambassadors->select('id','name','description','status','updated_at','created_at')->get();
		// Export file to type
		Excel::create('ambassadors', function($excel) use($ambassadors) {
			// Set the spreadsheet title, creator, and description
	        $excel->setTitle('Export List');
	        $excel->setCreator('Laravel')->setCompany('laravel.com');
	        $excel->setDescription('export file');

		    $excel->sheet('Sheet 1', function($sheet) use($ambassadors) {
				$sheet->fromArray($ambassadors);
		    });
		})->export($type);

	}

}
