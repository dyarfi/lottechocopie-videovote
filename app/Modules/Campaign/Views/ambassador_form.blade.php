@extends('Admin::layouts.template')
{{-- Page content --}}
@section('body')

<div class="page-header">
	<h1>{{ $mode == 'create' ? 'Create Ambassador' : 'Update Ambassador' }} <small>{{ $mode === 'update' ? $row->name : null }}</small></h1>
</div>

{!! Form::model($row,
	[
		'route' => ($mode == 'create') ? 'admin.ambassadors.create' : ['admin.ambassadors.update', $row->id],
		'files' => true,
		'autocomplete' => 'off'
	])
!!}

<div class="form-group{{ $errors->first('name', ' has-error') }}">
	{!! Form::label('name', 'Name'); !!}
	{!! Form::text('name',Input::old('name', $row->name),[
		'placeholder'=>'Enter the Ambassador name.',
		'name'=>'name',
		'id'=>'name',
		'class' => 'form-control']); !!}
	<span class="help-block">{{{ $errors->first('name', ':message') }}}</span>
</div>

<div class="form-group{{ $errors->first('slug', ' has-error') }}">
	{!! Form::label('slug', 'Slug'); !!}
	{!! Form::text('slug',Input::old('slug', $row->slug),[
		'placeholder'=>'Enter the Ambassador Slug.',
		'name'=>'slug',
		'id'=>'slug',
		'readonly'=>true,
		'class'=>'form-control']); !!}
	<span class="help-block">{{{ $errors->first('slug', ':message') }}}</span>
</div>

<div class="form-group{{ $errors->first('attribute', ' has-error') }}">
	<label for="attribute">Video</label>
	{!! Form::select('attribute', $videos, Input::get('attribute') ? Input::get('attribute') : Input::old('attribute', @$row->attribute),['class'=>'form-control']); !!}
	<span class="help-block">{{{ $errors->first('attribute', ':message') }}}</span>
</div>

<div class="form-group{{ $errors->first('description', ' has-error') }}">
	{!! Form::label('description', 'Description'); !!}
	{!! Form::textarea('description',Input::old('description', $row->description),[
		'placeholder'=>'Enter the Ambassador Description.',
		'name'=>'description',
		'id'=>'description',
		'class' => 'form-control ckeditor',
		'rows' => '4'
	]); !!}
	<span class="help-block">{{{ $errors->first('description', ':message') }}}</span>
</div>

<div class="form-group">
	@if ($row->image)
		<img src="{{ asset('uploads/'.$row->image) }}" alt="{{ $row->image }}" class="image-alt img-thumbnail" style="width:300px"/>
	@endif
	<div class="row">
		<div class="col-xs-6">
			{!! Form::label('image', ($row->image) ? 'Replace Image:' : 'Image:', ['class' => '']) !!}
			<label class="ace-file-input">
				{!! Form::file('image',['class'=>'form-controls','id'=>'id-input-file-2']) !!}
				<span class="ace-file-container" data-title="Choose">
					<span class="ace-file-name" data-title="No File ...">
						<i class=" ace-icon fa fa-upload"></i>
					</span>
				</span>
			</label>
		</div>
	</div>
</div>

<div class="form-group{{ $errors->first('status', ' has-error') }}">
	<label for="status">Status</label>
	<select id="status" name="status" class="form-control input-sm">
		<option value="">&nbsp;</option>
		@foreach (config('setting.status') as $config => $val)
			<option value="{{ $config ? $config : Input::old('status', $row->status) }}" {{ $config == $row->status ? 'selected' : '' }}>{{$val}}</option>
		@endforeach
	</select>
	<span class="help-block">{{{ $errors->first('status', ':message') }}}</span>
</div>

{!! Form::submit(ucfirst($mode).' Ambassador', ['class' => 'btn btn-primary btn-xs']) !!}

{!! Form::close() !!}

@stop
