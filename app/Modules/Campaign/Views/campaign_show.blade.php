@extends('Admin::layouts.template')

@section('body')
<div class="container-fluid">
    @if($row->name)
    <h4 class="red">Name</h4>
    <div class="row-fluid">
        {{ $row->name }}
    </div>
    @endif
    @if($row->description)
    <h4 class="red">Description</h4>
    <div class="row-fluid">
        {{ $row->description }}
    </div>
    @endif
    @if($row->image)
    <h4 class="red">Image &raquo; <span><i class="fa fa-desktop"></i></span></h4>
    <div class="row-fluid">
        <img src="{{ asset('uploads/'.$row->image) }}" class="img-responsive"/>
    </div>
    @endif
    @if($row->image_mobile)
    <h4 class="red">Image Mobile &raquo; <span><i class="fa fa-tablet"></i></span></h4>
    <div class="row-fluid">
        <img src="{{ asset('uploads/'.$row->image_mobile) }}" class="img-responsive"/>
    </div>
    @endif
    @if($row->embed)
    <h4 class="red">Embed</h4>
    <div class="row-fluid">
        {{ $row->embed }}
    </div>
    @endif    
    @if($row->status)
    <h4 class="red">Status</h4>
    <div class="row-fluid">
        {{ config('setting.status')[$row->status] }}
    </div>
    @endif
    @if($row->created_at)
    <h4 class="red">Created At</h4>
    <div class="row-fluid">
        {{ $row->created_at }}
    </div>
    @endif
    <hr/>
    <div class="row">
        <div class="col-md-5 col-xs-6">
            <a href="{{ route('admin.campaigns.index') }}" class="btn btn-info btn-xs">Back to all campaigns</a>
            <a href="{{ route('admin.campaigns.edit', $row->id) }}" class="btn btn-primary btn-xs">Edit Campaign</a>
            <a href="{{ route('admin.campaigns.create') }}" class="btn btn-warning btn-xs">Create Campaign</a>
        </div>
        <div class="col-md-5 col-xs-6 text-right">
            {!! Form::open([
                'method' => 'DELETE',
                'route' => ['admin.campaigns.trash', $row->id]
            ]) !!}
                {!! Form::submit('Delete this campaign?', ['class' => 'btn btn-danger btn-xs']) !!}
            {!! Form::close() !!}
        </div>
    </div>
</div>

@stop
