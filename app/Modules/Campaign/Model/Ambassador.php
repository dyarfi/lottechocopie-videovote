<?php namespace App\Modules\Campaign\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ambassador extends Model {

	// Soft deleting a model, it is not actually removed from your database.
    use SoftDeletes;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'ambassadors';

	/**
     * Fillable fields
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'name',
        'description',
        'requirement',
        'responsibility',
        'facility',
        'image',
        'attribute',
        'options',
        'end_date',
        'status'
    ];

    // Instead, a deleted_at timestamp is set on the record.
    protected $dates = ['deleted_at'];

    /**
     * Get the video associated with the ambassador.
     */
    public function video() {

        return $this->hasOne('App\Modules\Campaign\Model\Video','id','attribute');
        
    }

    // Scope query for active status field
    public function scopeActive($query) {

        return $query->where('status', 1)->orderBy('created_at','desc');

    }

    // Scope query for slug field
    public function scopeSlug($query, $string) {

        return $query->where('slug', $string)->firstOrFail();

    }

}
