<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Auth\Authenticable as AuthenticableTrait;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Participant extends Authenticatable
{
    use Notifiable;

    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
        'provider_id',
        'provider',
        'profile_url',
        'photo_url',
        'first_name',
        'last_name',
        'name',
        'username',
        'email',
        'password',
        'password_confirmation',
        'avatar',
        'about',
        'phone_number',
        'phone_home',
        'address',
        'region',
        'province',
        'urban_district',
        'sub_urban',
        'zip_code',
        'website',
        'gender',
        'age',
        'nationality',
        'id_number',
        'file_name',
        'verify',
        'completed',
        'logged_in',
        'last_login',
        'session_id',
        'join_date',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
	 * A user can have many tasks.
	 *
	 */
	public function vote()
	{
		return $this->hasOne('App\Modules\Participant\Model\Vote','participant_id');
	}
}
