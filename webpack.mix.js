let mix = require('laravel-mix').mix;

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management @ https://laravel.com/docs/5.4/mix
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

//mix.js('resources/assets/js/app.js', 'public/js')
   //.sass('resources/assets/sass/app.scss', 'public/css');
// mix.browserSync({
    // proxy: 'd3-app.dev'
// });

/*
mix.combine([
    'public/css/bootstrap.min.css',
    'public/css/font-awesome.min.css',
    'public/css/style.css',
    'public/css/swiper.css',
    'public/css/dark.css',
    'public/css/font-icons.css',
    'public/css/animate.css',
    'public/css/magnific-popup.css',
    'public/css/responsive.css',
    'public/css/additional.css'
],  'public/css/d3all.css').version();
mix.combine([
    'public/themes/ace-admin/css/bootstrap.min.css',
    'public/themes/ace-admin/css/ace.min.css',
    'public/themes/ace-admin/css/default.css'
],  'public/themes/ace-admin/css/d3all.css').options({
     processCssUrls: false
}).version();
*/

/*
// Admin Panel
mix.scripts([
    'public/themes/ace-admin/js/bootstrap.min.js',
    'public/themes/ace-admin/js/bootbox.min.js',
    'public/themes/ace-admin/js/jquery-ui.custom.min.js',
    'public/themes/ace-admin/js/jquery.ui.touch-punch.min.js',
    'public/themes/ace-admin/js/ace-elements.min.js',
    'public/themes/ace-admin/js/ace.min.js'
],  'public/themes/ace-admin/js/d3all.js').version();
*/

// Browsersync hot reload
mix.browserSync({
    proxy:'lottechocopieindonesia.test',
    // Open the site in Chrome & Firefox
    // browser: ["google chrome", "firefox"],
    browser: ["firefox"],
    // browser: ["google chrome"],
    // Don't show any notifications in the browser.
    notify: false,
    files: [
        "public/assets/js/**/*.js",
        "public/assets/css/**/*.css",
        "public/assets/images/**/*.jpg",
        "public/assets/images/**/*.png",
        "app/Http/**/*.php",
        "app/Modules/**/**/**/*.php",
        "storage/app/public/**/*.*",          
        "resources/assets/**/*.*",
        "resources/views/**/*.php",
        "resources/views/*.php",        
        "resources/lang/**/*.php",
    ]
});
mix.disableNotifications();

if (mix.inProduction()) {
    // Copy assets files
    mix.copy('node_modules/jquery/dist/jquery.min.js', 'public/js/jquery.min.js');
    mix.copy('node_modules/popper.js/dist/umd/popper.min.js', 'public/js/popper.min.js');
    mix.copy('node_modules/holderjs/holder.min.js', 'public/js/holder.min.js');
    mix.copy('node_modules/jrespond/js/jRespond.min.js', 'public/js/jRespond.min.js');    
    mix.copy('node_modules/bootstrap/dist/js/bootstrap.min.js', 'public/js/bootstrap.min.js');
    mix.copy('node_modules/owl.carousel/dist/owl.carousel.min.js', 'public/js/owl.carousel.min.js');
    // Copy modules assets directory
    //mix.copyDirectory('node_modules/gsap/src/minified','public/js/gsap');
    mix.copyDirectory('node_modules/@fancyapps/fancybox/dist','public/js/fancybox');
    mix.copyDirectory('node_modules/bootstrap/scss','resources/assets/scss/vendors/bootstrap/scss');
    mix.copyDirectory('node_modules/owl.carousel/src/scss','resources/assets/scss/vendors/owl.carousel/scss');
    mix.copyDirectory('node_modules/owl.carousel/src/img','public/images');
    // Resources assets files
    mix.copyDirectory('resources/assets/scss/vendors/fontawesome/webfonts', 'public/webfonts');
    //mix.copyDirectory('resources/assets/html/src/images', 'public/images');
    
     // SASS preprocess
     mix.sass(
        // SASS components
        'resources/assets/scss/main.scss',
        // CSS bundle SCSS into css app.bundle.css components bundled
        'public/css/app.bundle.css'
    ).options({
            processCssUrls: false,            
            postCss: [
                require('cssnano')({
                discardComments: {
                    removeAll: true,
                },
                }),
                //require('postcss-unprefix'),
                require('autoprefixer')({
                browsers: [
                    'last 15 versions',
                    'ie >= 10',
                ]}),
            ]            
        }).version();

    // Combine all
    mix.scripts([
        'public/js/jquery.min.js',
        'public/js/jquery-ias.js', 
        'public/js/popper.min.js',         
        'public/js/holder.min.js',
        'public/js/jRespond.min.js',
        'public/js/bootstrap.min.js',
        //'public/js/gsap/TweenMax.min.js',
        //'public/js/gsap/plugins/MorphSVG.min.js',
        'public/js/owl.carousel.min.js',
        'public/js/fancybox/jquery.fancybox.min.js',
        'public/js/app.js'
        // Bundle into app.bundle.js
    ],  'public/js/app.bundle.js').version();

    // JS Apps / React
    mix.js('resources/assets/js/app.js', 'public/js');

    // Only on production 
    // Set mix version
    mix.version();

    // JS
    mix.combine('public/css/app.bundle.css', 'public/css/app.bundle.css');
    // CSS
    // mix.combine('public/js/app.bundle.js', 'public/js/app.bundle.min.js');

} else {

    // SASS preprocess
    mix.sass(
        // SASS components
        'resources/assets/scss/main.scss',
        // CSS bundle SCSS into css app.bundle.css components bundled
        'public/css/app.bundle.css'
    ).options({processCssUrls: false}).version();

    // Combine all
    mix.scripts([
        'public/js/jquery.min.js',
        'public/js/jquery-ias.js', 
        'public/js/popper.min.js',         
        'public/js/holder.min.js',
        'public/js/jRespond.min.js',
        'public/js/bootstrap.min.js',
        //'public/js/gsap/TweenMax.min.js',
        //'public/js/gsap/plugins/MorphSVG.min.js',
        'public/js/owl.carousel.min.js',
        'public/js/fancybox/jquery.fancybox.min.js',
        'public/js/app.js'
        // Bundle into app.bundle.js
    ],  'public/js/app.bundle.js').version();
    
    // JS Apps / React
    mix.js('resources/assets/js/app.js', 'public/js');

    // Set mix version --- test mode
    // mix.version();

}